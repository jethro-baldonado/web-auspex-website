<div class="col s7">
    <div class="card-alert card-success hide">
        <div class="card-content valign-wrapper">
            <i class="material-icons" style="margin-right: 10px;">done</i>
            <div class="message"></div>
        </div>
    </div>
    <div class="card-alert card-error hide">
            <div class="card-content valign-wrapper">
                <i class="material-icons" style="margin-right: 10px;">error_outline</i>
                <div class="message"></div>
            </div>
        </div>
</div>
<div class="col s7 people-form" id="people">

<?php
    echo form_open_multipart('people/save',array('class'=>'form-builder', 'id'=> 'cover'));
    echo $form;
    ?>
    <div class="row">
        <div class="col s6 center-align">
            <button type="button" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s6 center-align">
            <button type="button" class="form-btn save-btn">SAVE</button>
        </div>
    </div>
<?php
    echo form_close();
?>
</div>
<div class="col s7 hide people-form" id="people-1">
<?php
    echo form_open_multipart('people/save',array('class'=>'form-builder', 'id'=> 'page-1'));
    echo $form1;
    ?>
    <div class="row">
        <div class="col s6 center-align">
            <button type="button" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s6 center-align">
            <button type="button" class="form-btn save-btn">SAVE</button>
        </div>
    </div>
<?php
    echo form_close();
?>
</div>
<div class="col s7 hide people-form" id="people-2">
<?php
    echo form_open_multipart('people/save',array('class'=>'form-builder', 'id'=> 'page-2'));
    echo $form2;
    ?>
    <div class="row">
        <div class="col s6 center-align">
            <button type="button" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s6 center-align">
            <button type="button" class="form-btn save-btn">SAVE</button>
        </div>
    </div>
<?php
    echo form_close();
?>
</div>
<div class="col s7 hide people-form" id="people-3">
<?php
    echo form_open_multipart('people/save',array('class'=>'form-builder', 'id'=> 'page-3'));
    echo $form3;
    ?>
    <div class="row">
        <div class="col s6 center-align">
            <button type="button" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s6 center-align">
            <button type="button" class="form-btn save-btn">SAVE</button>
        </div>
    </div>
<?php
    echo form_close();
?>
</div>
