<div class="col s7">
    <div class="card-alert card-success hide">
        <div class="card-content valign-wrapper">
            <i class="material-icons" style="margin-right: 10px;">done</i>
            <div class="message"></div>
        </div>
    </div>
    <div class="card-alert card-error hide">
            <div class="card-content valign-wrapper">
                <i class="material-icons" style="margin-right: 10px;">error_outline</i>
                <div class="message"></div>
            </div>
        </div>
</div>
<div class="col s7 people-form-update" id="people">

    <?php
    echo form_open_multipart('people/update_save',array('class'=>'form-builder', 'id'=>"cover"));
    echo $form;
    ?>
    <div class="row">
        <div class="col s4 center-align">
            <button type="button" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s4 center-align">
            <button type="button" class="form-btn save-btn">SAVE</button>
        </div>
        <div class="col s4 center-align">
            <button type="button" class="form-btn delete-btn">DELETE</button>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
</div>
<?php
for($i = 0; $i < count($formArr); $i++){
    ?>
    <div class="col s7 hide people-form-update" id="people-<?php echo $i+1; ?>">
        <?php
        $formId = "page-" . ($i + 1);
        echo form_open_multipart('people/update_save',array('class'=>'form-builder', 'id'=> $formId));
        echo $formArr[$i];
        ?>
        <div class="row">
            <div class="col s4 center-align">
                <button type="button" class="form-btn">PREVIEW</button>
            </div>
            <div class="col s4 center-align">
                <button type="button" class="form-btn save-btn">SAVE</button>
            </div>
            <div class="col s4 center-align">
                <button type="button" class="form-btn delete-btn">DELETE</button>
            </div>
        </div>
        <?php
        echo form_close();
        ?>
    </div>
    <?php
}
?>
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <h4 class="red-text text-darken-2">DELETE CONFIRMATION</h4>
    <p>Are you sure you want to delete this employee?</p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" id="agree-btn-emp">Agree</a>
  </div>
</div>
