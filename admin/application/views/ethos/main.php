<div class="col s7">
    <?php
        $result = $this->session->flashdata('status');
        if(count($result)){
            if($result['status']){
            ?>
            <div class="card-alert card-success">
                <div class="card-content valign-wrapper">
                    <i class="material-icons" style="margin-right: 10px;">done</i>
                    <?php echo $result['message']; ?>
                </div>
            </div>
            <?php
            }else{
            ?>
            <div class="card-alert card-error">
                <div class="card-content valign-wrapper">
                    <i class="material-icons" style="margin-right: 10px;">error_outline</i>
                    <?php echo $result['message']; ?>
                </div>
            </div>
            <?php
            }
        }
    ?>

<?php
    echo form_open_multipart($submitLink,array('class'=>'form-builder', 'id' => 'ethos-form'));
    echo $form;
    ?>
    <div class="row">
        <div class="col s6 center-align">
            <button type="button" class="form-btn" id="preview-btn">PREVIEW</button>
        </div>
        <div class="col s6 center-align">
            <button type="submit" class="form-btn" id="save-btn">SAVE</button>
        </div>
    </div>
<?php
    echo form_close();
?>
</div>
