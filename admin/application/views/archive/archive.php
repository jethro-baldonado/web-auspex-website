<div class="col s7 archives" data-date="<?php echo $date; ?>">
    <div class="card-alert card-success hide">
        <div class="card-content valign-wrapper">
            <i class="material-icons" style="margin-right: 10px;">done</i>
            <div class="message"></div>
        </div>
    </div>
    <div class="card-alert card-error hide">
        <div class="card-content valign-wrapper">
            <i class="material-icons" style="margin-right: 10px;">error_outline</i>
            <div class="message"></div>
        </div>
    </div>
<?php
    if($form != ""){
    echo form_open_multipart('news/update',array('class'=>'form-builder'));
        echo $form;
    ?>
    <div class="row">
        <div class="col s4 center-align">
            <button type="button" id="btn-news-preview" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s4 center-align">
            <button type="button" id="btn-news-delete" class="form-btn">DELETE</button>
        </div>
        <div class="col s4 center-align">
            <button type="button" id="btn-news-save" class="form-btn">SAVE</button>
        </div>
    </div>
<?php

    echo form_close();
}else{
    ?>
    <div class="card-alert card-success">
        <div class="card-content valign-wrapper">
            <i class="material-icons">info_outline</i>
            <div class="message" style="margin-left: 10px;">No archived news.</div>
        </div>
    </div>
    <?php
}
?>
</div>
<div id="modal1" class="modal bottom-sheet">
  <div class="modal-content">
    <h4 class="red-text text-darken-2">DELETE CONFIRMATION</h4>
    <p>Are you sure you want to delete this article?</p>
  </div>
  <div class="modal-footer">
    <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" id="agree-btn">Agree</a>
  </div>
</div>
