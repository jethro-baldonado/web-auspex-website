
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Auspex</title>
  <base href="/">

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="favicon.png">
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/auspex-website/assets/js/fullpage/jquery.fullpage.css">
  <link rel="stylesheet" type="text/css" href="/auspex-website/assets/fonts/style.css">
  <link rel="stylesheet" type="text/css" href="/auspex-website/assets/stylesheets/css/main.css">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
  <script src="/auspex-website/assets/js/fullpage/jquery.fullpage.js"></script>
</head>
<body>
    <?php
    $date_published = date('Y-m-d', strtotime($date_published));
    $dateArr = explode('-', $date_published);
    ?>
    <div class="wrapper">
        <div class="bg" style="background-image: url(/auspex/src/assets/images/news.png)"></div>
        <div class="content">
            <div class="row" style="width: 100%;">
                <div class="col s12 m2">
                    <div class="date-holder-article">
                        <div class='year'><?php echo $dateArr[0];?></div>
                        <div class="month"><?php echo $dateArr[1]; ?></div>
                        <div class="day"><?php echo $dateArr[2]; ?></div>
                    </div>
                </div>
                <div class="col s12 m8">
                    <div class="news-detail-container">
                        <div class="thumb">
                            <img src="<?php echo base_url($thumbnail); ?>">
                        </div>
                        <div class="title">
                            <h2><?php echo $title; ?></h2>
                            <div class="social-media">
                                <div class="action-group" [@slideInOut]="actionShow">
                                    <button ngxClipboard [cbContent]="currentPage">
                                        <span class="icon-copy"></span>
                                    </button>
                                    <button shareButton="facebook">
                                        <span class="icon-facebook"></span>
                                    </button>
                                    <button shareButton="twitter">
                                        <span class="icon-twitter"></span>
                                    </button>
                                </div>
                                <div class="toggler">
                                    <button *ngIf="actionShow == 'in'" (click)="showActionGroup()" ><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                                    <button *ngIf="actionShow == 'out'" (click)="hideActionGroup()"><i class="fa fa-times" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                        <div class="inner-content">
                            <p><?php echo $content; ?></p>
                        </div>
                    </div>
                </div>
                <div class="col s12 m2 side-controls">
                    <div class="close-btn">
                        <div class="scroll-container">
                            <a routerLink="/news" class="scroll-btn"><span class="icon-close"></span></a>
                        </div>
                    </div>
                    <div class="navigation-btn">
                        <div class="row">
                            <a [routerLink]="prevNews" class="scroll-btn">Previous Article</a>
                        </div>
                        <div class="row">
                            <a [routerLink]="nextNews" class="scroll-btn">Next Article</a>
                        </div>
                    </div>
                    <div class="home-btn">
                        <a routerLink="" class="scroll-btn"><span class="icon-auspex-home"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.2.2/web-animations.min.js"></script>
</body>
</html>
