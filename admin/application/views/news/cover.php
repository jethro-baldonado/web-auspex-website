<div class="col s7">
<?php
    echo form_open_multipart('news/cover_upload',array('class'=>'form-builder'));
    echo $form;
    ?>
    <div class="row">
        <div class="col s6 center-align">
            <button type="button" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s6 center-align">
            <button type="submit" class="form-btn">SAVE</button>
        </div>
    </div>
<?php
    echo form_close();
?>
</div>
