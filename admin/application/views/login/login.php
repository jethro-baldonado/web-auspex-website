<div class="valign-wrapper fixed-height login">
    <div class="row">
        <div class="col s12 m4 offset-m4">
            <div class="brand-logo col s12">
                <span class="icon-hero-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>
            </div>
            <div class="errors">
                <div class="row">
                    <div class="col s12">
                        <p class="red-text"><?php echo $this->session->flashdata('message'); ?></p>
                    </div>
                </div>
            </div>
            <div class="login-form">
                <?php echo form_open('login/auth',array(
                    'method' => 'post'
                )); ?>
                <div class="row">
                    <div class="col s12">
                        <input type="text" placeholder="Username" name="email" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <input type="password" placeholder="Password" name="password" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col s6">
                        <p>
                            <input type="checkbox" class="filled-in" id="filled-in-box"/>
                            <label for="filled-in-box">Remember me</label>
                        </p>
                    </div>
                    <div class="col s6">
                        <p class="right-align">
                            <a href="">Forgot Password?</a>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <button type="submit">LOGIN</button>
                    </div>
                </div>

                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
