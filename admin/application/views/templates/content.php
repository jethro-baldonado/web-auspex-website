<div class="row nav-head valign-wrapper">
    <div class="col s12 m3">
        <a href="">
            <span class="icon-hero-logo"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></span>
        </a>
    </div>
    <div class="col m2 offset-m6 right-align"><?php echo date('D, F j, Y'); ?></div>
    <div class="col m1 center-align"><a href="<?php echo base_url('login/logout'); ?>">Logout</a></div>
</div>
<div class="row">
    <div class="col m2 main-nav">
        <div class="sidebar">
            <div class="profile-info center-align">
                <div class="row">
                    <img src="<?php echo base_url($_SESSION['userinfo'][0]->display_picture) ?>" alt="" class="circle" style="width:75px;height: 75px;">
                </div>
                <div class="row">
                    <span class="blue-green"><?php echo $_SESSION['userinfo'][0]->first_name . ' ' . $_SESSION['userinfo'][0]->last_name; ?></span>
                </div>
            </div>
            <div class="nav">
                <ul>
                    <li class="<?php echo $page == 'ethos' || $page == 'globalpresence' ? 'active' : ''; ?>"><a href="">Our People</a>
                        <ul class="sub-menu">
                            <li class="<?php echo $page == 'ethos' ? 'active' : ''; ?>"><a href="<?php echo base_url('ethos'); ?>">Ethos</a></li>
                            <li class="<?php echo $page == 'globalpresence' ? 'active' : ''; ?>"><a href="<?php echo base_url('globalpresence'); ?>">Global Presence</a></li>
                            <li><a href="<?php echo base_url('people'); ?>">Our People</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo $page =='web' || $page == 'mobile' || $page == 'other' ? 'active' : ''; ?>"><a href="">Solutions</a>
                        <ul class="sub-menu">
                            <li class="<?php echo $page == 'web' ? 'active' : ''; ?>"><a href="<?php echo base_url('solutions/web'); ?>">Web</a></li>
                            <li class="<?php echo $page == 'mobile' ? 'active' : ''; ?>"><a href="<?php echo base_url('solutions/mobile') ?>">Mobile</a></li>
                            <li class="<?php echo $page == 'other' ? 'active' : ''; ?>"><a href="<?php echo base_url('solutions/other') ?>">Others</a></li>
                        </ul>
                    </li>
                    <li class="<?php echo $page == 'news' || $page == 'archive' ? 'active' : ''; ?>">
                        <a href="">What's New</a>
                        <ul class="sub-menu">
                            <li class="<?php echo $page == 'news' ? 'active' : ''; ?>"><a href="<?php echo base_url('news'); ?>">Recent</a></li>
                            <li class="<?php echo $page == 'archive' ? 'active' : ''; ?>"><a href="<?php echo base_url('archives'); ?>">Archives</a></li>
                        </ul>
                    </li>
                    <li><a href="">Contact Us</a>
                        <ul class="sub-menu">
                            <li><a href="">Partners</a></li>
                            <li><a href="">Say Hi</a></li>
                            <li><a href="">Careers</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col m10 content-container">
        <div class="col s12 content-nav">
            <?php echo $nav; ?>
        </div>
        <div class="col s12 content-inner">
            <?php echo $content; ?>
        </div>
    </div>
</div>
