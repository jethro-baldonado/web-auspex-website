<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.99.0/js/materialize.min.js"></script>
<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=ad4ivkbqxmkdozu5tg2ycdgd0tbopp3qplbyh76unze1eta7"></script>
<script>
    tinymce.init({
        selector:'.editor',
        height: 500,
        menubar : false,
        plugins: [
        "advlist autolink lists link charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime table contextmenu paste image"],
        toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });
    $(document).ready(function(){
        $('.content-type, .color-options').material_select();
        $('.nav > ul > li > a').click(function(){
            $(this).parent('li').children('.sub-menu').toggle();
            return false;
        });
        $(document).on('click','.browse-btn', function(){
            $(this).parents('.uploader').find('.upload-file').trigger('click');

        });
        $(document).on('change', '.upload-file', function(){
            $(this).parents('.uploader').find('.upload-path').val( $(this).val().replace(/C:\\fakepath\\/i, ''));
        });
        $('#filled-in-box').click(function(){
            if($(this).prop('checked')){
                var dateToday = new Date();
                var dd = dateToday.getDate();
                var mm = dateToday.getMonth()+1;
                var yy = dateToday.getFullYear();
                $('.date-selector-month').val(mm);
                $('.date-selector-day').val(dd);
                $('.date-selector-year').val(yy);
            }else{
                $('.date-selector-month').prop('selectedIndex', 0);
                $('.date-selector-day').prop('selectedIndex', 0);
                $('.date-selector-year').prop('selectedIndex', 0);
            }
        });
        $('.date-selector-month').on('change', function(){
            $('.date-selector-day').empty();
            $('.date-selector-day').append('<option disabled="disabled">Day</option>');
            switch($(this).val()){
                case "1":
                case "3":
                case "5":
                case "7":
                case "8":
                case "10":
                case "12":
                    for(let i = 1; i <= 31; i++){
                        $('.date-selector-day').append(
                            '<option value="'+i+'">' + i + '</option>'
                        );
                    }
                break;
                case "4":
                case "6":
                case "9":
                case "11":
                for(let i = 1; i <= 30; i++){
                    $('.date-selector-day').append(
                        '<option value="'+i+'">' + i + '</option>'
                    );
                }
                break;
                default:
                let days = $('.date-selector-year').val() % 4 == 0 ? 29 : 28;
                for(let i = 1; i <= days; i++){
                    $('.date-selector-day').append(
                        '<option value="'+i+'">' + i + '</option>'
                    );
                }
                break;
            }
        });
        $('.date-nav li a').click(function(){
            event.preventDefault();
            var href = $(this).attr('href');
            href += '/' + $('#arch-year').val();
            window.location.href = href;
        });
        $('#arch-day, #arch-year, #person-list, #solution-list').material_select();
        $('#arch-day').on('change', function(){
            if($(this).val() == 'all'){
                window.location.href = $('.date-nav li.active a').attr('href') + '/' + $('#arch-year').val() + '/';
            }else{
                window.location.href = $('.date-nav li.active a').attr('href') + '/' + $('#arch-year').val() + '/' + $(this).val();

            }
        });
        $('.accordion .acc-content').hide();
        $('.accordion .acc-label').on('click', function(){
            $('html,body').animate({scrollTop : $(this).offset().top},'fast');
            var accVisible = $(this).parents('.accordion').find('.acc-content').is(':visible');
            $('.acc-content').hide();
            $('.accordion .acc-label i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            $('.accordion').removeClass('collapsed');
            if(accVisible){
                $(this).children('.fa').removeClass('fa-chevron-down').addClass('fa-chevron-up');
            }else{
                $(this).children('.fa').removeClass('fa-chevron-up').addClass('fa-chevron-down');
                $(this).parents('.accordion')
                    .addClass('collapsed')
                    .find('.acc-content').show();
            }
        });

        $('.archives #btn-news-save').click(function(){
            event.preventDefault();
            var newsId = $('.collapsed').data('news-id');
            if(newsId){
                var formHref = $(this).parents('form').attr('action');
                var formData = new FormData();
                formData.append('news-id', newsId);
                formData.append('content', tinyMCE.get('content' + newsId).getContent());
                $('.collapsed input').each(function(){
                    if($(this).attr('type') != 'file'){
                        formData.append($(this).attr('name'), $(this).val());
                    }else{
                        formData.append($(this).attr('name'), $(this).prop('files')[0]);
                    }
                });
                if(!validateFields('.collapsed')){
                    $.ajax({
                        'url' : formHref,
                        'method' : 'POST',
                        mimeType : "multipart/form-data",
                        contentType : false,
                        processData : false,
                        'data' : formData,
                        success : function(response){
                            var res = JSON.parse(response);
                            $('.card-error, .card-success').addClass('hide');
                            if(res.status){
                                $('.card-success').find('.message').html(res.message);
                                $('.card-success').removeClass('hide');
                                resetFields('.collapsed');
                            }else{
                                $('.card-error').find('.message').html(res.message);
                                $('.card-error').removeClass('hide');
                            }
                            $('html,body').animate({scrollTop : 0},'fast');
                        }
                    });
                }
            }else{

                $('.card-error').find('.message').html('Please select an article to update.');
                $('.card-error').removeClass('hide');
            }


            return false;
        });
        $('#recent-news #preview-btn').click(function(){
            var formData = new FormData();
            formData.append('news-title', $('#news-title').val());
            formData.append('content', tinyMCE.get('content').getContent());
            formData.append('image-thumb', $('.upload-file').prop('files')[0]);
            var day = $('.date-selector-day').val() <= 9 ? "0" + $('.date-selector-day').val() : $('.date-selector-day').val();
            var month = $('.date-selector-month').val() <= 9 ? "0" + $('.date-selector-month').val() : $('.date-selector-month').val();
            var date = $('.date-selector-year').val() + '-' + month + '-' + day;
            formData.append('date', date);
            $.ajax({
                'url' : '<?php echo base_url('news/save_preview'); ?>',
                'method' : 'POST',
                mimeType : "multipart/form-data",
                contentType : false,
                processData : false,
                data : formData,
                success : function(response){
                    if(response > 0){
                        var win = window.open('<?php echo base_url('news/preview/'); ?>' + response);
                        win.focus();
                    }else{

                    }
                }
            })
        });
        $('.archives #btn-news-preview').click(function(){
            var parentElement = $('.collapsed .form-builder');
            var formData = new FormData();
            var newsId = $('.collapsed').data('news-id');
            formData.append('news-id', newsId);
            formData.append('content', tinyMCE.get('content' + newsId).getContent());
            $('.collapsed input').each(function(){
                if($(this).attr('type') != 'file'){
                    formData.append($(this).attr('name'), $(this).val());
                }else{
                    formData.append($(this).attr('name'), $(this).prop('files')[0]);
                }
            });

            var date = $('.archives').data('date');

            formData.append('date',date);

            $.ajax({
                'url' : '<?php echo base_url('news/save_preview'); ?>',
                'method' : 'POST',
                mimeType : "multipart/form-data",
                contentType : false,
                processData : false,
                'data' : formData,
                success : function(response){
                    if(response > 0){
                        var win = window.open('<?php echo base_url('news/preview/'); ?>' + response);
                        win.focus();
                    }else{

                    }
                }
            });
        });
        $('.archives #btn-news-delete').click(function(){
            var newsId = $('.collapsed').data('news-id');
            if(newsId){
                $('#modal1').modal('open');
            }else{
                $('.card-error, .card-success').addClass('hide');
                $('.card-error').find('.message').html('Please select an article to update.');
                $('.card-error').removeClass('hide');
            }
            $('html,body').animate({scrollTop : 0},'fast');
        });
        $('#agree-btn').click(function(){
            var newsId = $('.collapsed').data('news-id');
            $.ajax({
                'url' : '<?php echo base_url('news/delete'); ?>/' + newsId,
                'method' : 'GET',
                success : function(response){
                    var res = JSON.parse(response);
                    $('.card-error, .card-success').addClass('hide');
                    if(res.status){
                        $('.card-success').find('.message').html(res.msg);
                        $('.card-success').removeClass('hide');
                    }else{
                        $('.card-error').find('.message').html(res.msg);
                        $('.card-error').removeClass('hide');
                    }
                    $('html,body').animate({scrollTop : 0},'fast');
                    $('.collapsed').remove();
                }
            })
        });
        $('#recent-news #save-btn').click(function(){
            if(!validateFields('#recent-news')){
                return true;
            }
            return false;
        });
        $('.person-nav a').click(function(event){
            event.preventDefault();
            var link = $(this).text();
            $(this).parents('.person-nav').children('li').removeClass('active');
            $(this).parents('li').addClass('active');
            $('.people-form, .people-form-update').hide();
            switch(link){
                case 'Page 1':
                $('#people-1').removeClass('hide').show();
                break;
                case 'Page 2':
                $('#people-2').removeClass('hide').show();
                break;
                case 'Page 3':
                $('#people-3').removeClass('hide').show();
                break;
                case 'Cover':
                default :
                $('#people').removeClass('hide').show();
                break;
            }
            return false;
        });
        $('.people-form .form-builder .save-btn').click(function(){
            var formId = "#" + $(this).parents('.form-builder').attr('id');
            var personId = $(formId).data('person-id');

            if(!validateFields(formId)){
                var formData = getValues(formId);
                var type;
                switch(formId){
                    case '#cover':
                    type = '1';
                    break;
                    case '#page-1':
                    type = '2';
                    break;
                    case '#page-2':
                    type = '2';
                    break;
                    case '#page-3':
                    type = '2';
                    break;
                }
                formData.append('type', type);
                if(personId){
                    formData.append('id',personId);
                }
                $.ajax({
                    'url' : $(formId).attr('action'),
                    'method' : 'POST',
                    mimeType : "multipart/form-data",
                    contentType : false,
                    processData : false,
                    'data' : formData,
                    success : function(response){
                        var res = JSON.parse(response);
                        $('.card-error, .card-success').addClass('hide');
                        if(res.status){
                            $('.card-success').find('.message').text(res.message);
                            $('.card-success').removeClass('hide');
                            $('#page-1, #page-2, #page-3').data('person-id', res.personId);
                            console.log(res.message);
                        }else{
                            $('.card-error').find('.message').text(res.message);
                            $('.card-error').removeClass('hide');
                            console.log(res.message);
                        }

                        $('html,body').animate({scrollTop : 0},'fast');
                    }

                });
            }else{
                return false;
            }

        });
        $('#person-list').on('change', function(){
            event.preventDefault();
            if($(this).val() > 0){
                window.location.href = ("<?php echo base_url('people/update'); ?>/" + $(this).val());
            }else{
                window.location.href = "<?php echo base_url('people/create'); ?>";
            }
            return false;
        });
        $('.people-form-update .save-btn').click(function(){
            var formId = "#" + $(this).parents('.form-builder').attr('id');
            var personId = $(formId).find('#person-id').val();
            console.log(validateFields(formId));
            if(!validateFields(formId)){
                var formData = getValues(formId);
                var type;
                switch(formId){
                    case '#cover':
                    type = '1';
                    break;
                    case '#page-1':
                    type = '2';
                    break;
                    case '#page-2':
                    type = '2';
                    break;
                    case '#page-3':
                    type = '2';
                    break;
                }
                formData.append('type', type);
                formData.append('empId', $('#people #person-id').val());

                console.log(formData);
                $.ajax({
                    'url' : $(formId).attr('action'),
                    'method' : 'POST',
                    mimeType : "multipart/form-data",
                    contentType : false,
                    processData : false,
                    'data' : formData,
                    success : function(response){
                        var res = JSON.parse(response);
                        $('.card-error, .card-success').addClass('hide');
                        if(res.status){
                            $('.card-success').find('.message').text(res.message);
                            $('.card-success').removeClass('hide');
                            console.log(res.message);
                        }else{
                            $('.card-error').find('.message').text(res.message);
                            $('.card-error').removeClass('hide');
                            console.log(res.message);
                        }

                        $('html,body').animate({scrollTop : 0},'fast');
                    }
                });
            }
        });
        $('#agree-btn-emp').click(function(){

            var link = $('.person-nav li.active a').text();
            formId = "";
            switch(link){
                case 'Page 1':
                    formId = "#page-1";
                break;
                case 'Page 2':
                    formId = "#page-2";
                break;
                case 'Page 3':
                    formId = "#page-3";
                break;
                case 'Cover':
                default :
                    formId = "#cover";
                break;
            }
            var personId = $(formId).find('#person-id').val();
            switch(formId){
                case '#cover':
                type = '1';
                break;
                case '#page-1':
                type = '2';
                break;
                case '#page-2':
                type = '2';
                break;
                case '#page-3':
                type = '2';
                break;
            }
            if(type == 1){
                $.ajax({
                    url : '<?php echo base_url('people/delete'); ?>/' + personId ,
                    'method' : 'GET',
                    success : function(response){
                        var res = JSON.parse(response);
                        $('.card-error, .card-success').addClass('hide');
                        if(res.status){
                            $('.card-success').find('.message').text(res.message);
                            $('.card-success').removeClass('hide');
                            window.location.href = "<?php echo base_url('people/create'); ?>"
                        }else{
                            $('.card-error').find('.message').text(res.message);
                            $('.card-error').removeClass('hide');
                            console.log(res.message);
                        }
                    }
                })
            }else{
                var pageId = $(formId).find('[name="people-page-id"]').val();
                $.ajax({
                    url : '<?php echo base_url('people/delete_page'); ?>/' + pageId,
                    'method' : 'GET',
                    success : function(response){
                        var res = JSON.parse(response);
                        $('.card-error, .card-success').addClass('hide');
                        if(res.status){
                            $('.card-success').find('.message').text(res.message);
                            $('.card-success').removeClass('hide');
                            clearFields(formId);
                        }else{
                            $('.card-error').find('.message').text(res.message);
                            $('.card-error').removeClass('hide');
                            console.log(res.message);
                        }

                        $('html,body').animate({scrollTop : 0},'fast');
                    }
                })
            }
        });
        $('.people-form-update .delete-btn').click(function(){
            $('#modal1').modal('open');
        });
        $(document).on('change','#solution-main .content-type', function(){
            $(this).children('option:checked').each(function(){
                if($(this).val() == 1){
                    $('#solution-main #image-thumb').css('display','block');
                    $('#solution-main .graph-container').hide();
                }else if($(this).val() == 2){
                    $('#solution-main #image-thumb').css('display','none');
                    $('#solution-main .graph-container').show();
                }
            });
        });
        var base_url = '<?php echo base_url(); ?>';
        $('.solution-form-container').ready(function(){
            $.ajax({
                'method' : 'GET',
                'url' : base_url + '/solutions/render_form/cover/'+$('.solution-form-container').data('last-id'),
                success : function(response){
                    $('.solution-form-container').append(response);
                    tinymce.init({selector:'.editor'});
                    $('select.content-type, select.color-options').material_select();
                    $('#solution-main .graph-container').hide();
                }
            })
        });
        $('.solution-nav a').click(function(){
            tinymce.remove('.solution-form-container .editor');
            $('.solution-form-container').empty();
            type = $(this).text().toLowerCase();
            $.ajax({
                'method' : 'GET',
                'url' : base_url + '/solutions/render_form/' + type + '/' + $('.solution-form-container').data('last-id'),
                success : function(response){
                    $('.solution-form-container').append(response);
                    tinymce.init({selector:'.editor'});
                    $('select.content-type').material_select();
                    $('#solution-main .graph-container').hide();
                }
            })
        });
        var graphTypeValue = 0;
        var graphRow = 0;
        $(document).on('click', '.graph-selector li:lt(3) a', function(){
            graphRow = 0;
            $('.graph-selector li:lt(3)').removeClass('active');
            $(this).parent('li').addClass('active');
            var graphType = $(this).text().toLowerCase();
            switch(graphType){
                case "pie":
                    graphTypeValue = 3;
                break;
                case 'bars':
                    graphTypeValue = 2;
                break;
                case 'lines':
                    graphTypeValue = 1;
                break;
            }
            $('.variable-list').empty();
            return false;
        });
        $(document).on('click', '.add-graph-value', function(){
            graphRow++;
            $.ajax({
                'method' : 'GET',
                'url' : base_url + '/solutions/request_graph_format/' + graphTypeValue + '/' + graphRow,
                success : function(response){
                    $('.variable-list')
                        .append(response);
                }
            });
            return false;
        });
        var solutionType = '';
        $('#solution-list').on('change', function(){
            var solutionType = $('.sidebar .sub-menu li.active a').text().toLowerCase();
            window.location.href = base_url + '/solutions/'+ solutionType + '/' + $(this).val();
        });
        $(document).on('click', '.solution-form-container .form-builder #save-solution', function(){
            event.preventDefault();
            var formData = new FormData();
            var parts = window.location.href.toString().split('/');
            var lastSegment = parts.pop() || parts.pop();  // handle potential trailing slash
            //Contents
            formData.append('solution-type', lastSegment);
            formData.append('solution-id', $('.form-builder').data('page-id'))
            formData.append('title', $('.solution-form-container .form-builder input:eq(0)').val());
            formData.append('content', tinyMCE.get('content').getContent());
            formData.append('content-type', $('.solution-form-container select.content-type').val());

            formData.append('type', $('.solution-form-container .form-builder').data('type'));
            if(document.getElementsByClassName('color-options')){
                formData.append('bg_color', $('.solution-form-container select.color-options').val());
            }
            if($('#solution-main select.content-type').val() == 1 || $('#solution-main select.content-type').val() == undefined){
                formData.append('upload', $('.form-builder .upload-file').prop('files')[0]);

                console.log($('.form-builder .upload-file').prop('files')[0]);
                console.log($('.form-builder .upload-file').html());
            }else{
                //Chart Details
                var varList = $('.variable-list');
                var index = 0;
                var graphContent = {};
                var labelList = [];
                var colorList = [];
                var valueList = [];
                varList.children('[class^=var-list-item-]').each(function(){
                    var listContent = {};
                    var label = $(this).find('.label').val();
                    var color = $(this).find('.color').val();
                    var value = $(this).find('.value').val();
                    labelList.push(label);
                    colorList.push(color);
                    valueList.push(value);
                    if(graphTypeValue == 2){
                        var labelLocation = $(this).find('.filled-in').prop('checked');
                        listContent['location'] = labelLocation;
                    }else if(graphTypeValue == 1){
                        var strokeRange = $(this).find('#stroke-range').val();
                        listContent['stroke'] =  strokeRange;
                    }
                    index++;
                });
                graphContent['labels'] = labelList;
                graphContent['datasets'] = {};
                graphContent['datasets']['label'] = $('.solution-form-container .form-builder input:eq(0)').val();
                graphContent['datasets']['data'] = valueList;
                graphContent['datasets']['backgroundColor'] = colorList;
                formData.append('chartData', JSON.stringify(graphContent));
                formData.append('graph-type', $('.graph-selector li.active a').text().toLowerCase());
            }
            $.ajax({
                'method' : 'POST',
                'url' : base_url + '/solutions/create',
                processData: false,
                contentType: false,
                data : formData,
                success : function(response){
                    var result = JSON.parse(response);
                    if(result[0].status && result[0].redirectionUrl){
                        window.location.href = result[0].redirectionUrl;
                    }else{
                        console.log(result);
                    }
                }
            })
            return false;
        });
    });
    function createMessages(msg, status){

    }
    function validateFields(formId){
        var length = 0;
        var errors = 0;
        $(formId).find('input, textarea, select').each(function(){
            var attr = $(this).attr('name');
            if(attr != ''){
                length++;
            }
            if($(this).is('input')){
                if($(this).val() != ''){

                }else{

                    if($(this).attr('type') == 'file'){
                        if($(this).next('div').find('input').val() == ''){
                            errors++;
                            $(this).attr('style','border-color:#ef5350 !important');
                            $(this).addClass('tooltipped').attr('data-tooltip','Field is required').attr('data-position','top');
                        }
                    }else if($(this).attr('type') == 'hidden'){

                    }else{
                        $(this).attr('style','border-color:#ef5350 !important');
                        $(this).addClass('tooltipped').attr('data-tooltip','Field is required').attr('data-position','top');
                        errors++;
                    }
                }
            }else if($(this).is('textarea')){
                if($(this).hasClass('editor')){
                    var id = $(this).attr('id');
                    if(tinyMCE.get(id).getContent() == ''){
                        $(this).prev().attr('style', 'border:1px solid #ef5350')
                            .addClass('tooltipped').attr('data-tooltip','Field is required').attr('data-position','top');
                        errors++;
                    }
                }
            }else if($(this).is('select')){

                if($(this).val() == null){
                    $(this).attr('style','border-color:#ef5350');
                    $(this).addClass('tooltipped').attr('data-tooltip','Field is required').attr('data-position','top');
                    errors++;
                }
            }
        });
        $('.tooltipped').tooltip({delay : 50});
        return errors > 0;
    }
    function resetFields(formId){
        $(formId).find('input, textarea, select').each(function(){
            var attr = $(this).attr('name');
            if($(this).is('input')){
                    $(this).removeAttr('style');
                    $(this).addClass('tooltipped').removeAttr('data-tooltip').removeAttr('data-position');

            }else if($(this).is('textarea')){
                if($(this).hasClass('editor')){
                    var id = $(this).attr('id');

                        $(this).prev().removeAttr('style')
                            .removeClass('tooltipped').removeAttr('data-tooltip').removeAttr('data-position');

                }
            }else if($(this).is('select')){

                    $(this).removeAttr('style');
                    $(this).removeClass('tooltipped').removeAttr('data-tooltip').removeAttr('data-position');

            }
        });
    }
    function clearFields(formId){
        $(formId).find('input, textarea, select').each(function(){
            if($(this).is('textarea')){
                if($(this).hasClass('editor')){
                    var id = $(this).attr('id');
                    tinyMCE.get(id).setContent('');
                }
            }else{
                $(this).val('');
            }
        });
    }
    function getValues(formId){
    var formData = new FormData();
        $(formId).find('input, textarea, select').each(function(){
            if($(this).attr('type') == 'file'){
                formData.append('image-thumb', $(this).prop('files')[0]);
            }else{
                formData.append($(this).attr('name'), $(this).val());
            }
            if($(this).is('textarea')){
                if($(this).hasClass('editor')){
                    var id = $(this).attr('id');
                    console.log(tinyMCE.get(id).getContent());
                    if(tinyMCE.get(id).getContent() != ''){
                        formData.append($(this).attr('name'), tinyMCE.get(id).getContent());
                    }
                }
            }
        });
        return formData;
    }
</script>
</body>
</html>
