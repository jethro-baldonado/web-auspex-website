<?php

class Pages_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    public function save($inputArr){
        $this->db->where('menu_id', $inputArr['menu_id']);
        $query = $this->db->get('pages');
        if($query->num_rows() > 0){
            $this->db->where('menu_id', $inputArr['menu_id']);
            return $this->db->update('pages', $inputArr);
        }else{
            return $this->db->insert('pages', $inputArr);
        }
    }
    public function getPage($menu_id){
        $this->db->where('menu_id', $menu_id);
        $query = $this->db->get('pages');
        if($query->num_rows() > 0){
            return $query->result_array()[0];
        }else{
            return 0;
        }
    }
    public function getAllPages(){
        $this->db->where('menu_id != 5');
        $this->db->order_by('menu_id', 'ASC');
        $query = $this->db->get('pages');
        if($query->num_rows() > 0){
            return $query->result_array();
        }else{
            return 0;
        }
    }
}
