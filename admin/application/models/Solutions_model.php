<?php

class Solutions_model extends CI_Model{

    public function __construct(){
        parent::__construct();
        $this->load->database();
    }
    public function getSolutionList(){
        $this->db->select('title,solution_id');
        $result = $this->db->get('solutions');
        return $result->result();
    }
    public function save($inputArr){
        $data = array();

        if($inputArr['page'] == 'cover'){
            $data['title'] = $inputArr['title'];
            $data['description'] = $inputArr['description'];
            $data['image'] = $inputArr['image'];
            $data['solution_type'] = $inputArr['solution_type'];
            $data['bg_color'] = $inputArr['bg_color'];
            if($this->db->insert('solutions', $data)){
                return $this->db->insert_id();
            }else{
                return 0;
            }

        }else{
            $page = str_replace('page-','',$inputArr['page']);
            $data['page_name'] = $inputArr['title'];
            $data['description'] = $inputArr['description'];
            json_decode($inputArr['chart_data']);
            if(json_last_error() == JSON_ERROR_NONE){
                $data['chart_data'] = json_decode($inputArr['chart_data']);
            }else{
                $data['chart_data'] = $inputArr['chart_data'];
            }
            $data['solution_id'] = $inputArr['solution_id'];
            $data['page_id'] = $page;
            $data['graph_type'] = $inputArr['graph_type'];
            if($this->db->insert('solution_pages', $data)){
                return true;
            }else{
                return false;
            }
        }
    }
    public function update($inputArr){
        $data = array();
        $this->db->where('solution_id', $inputArr['solution_id']);
        if($inputArr['page'] == 'cover'){
            $data['title'] = $inputArr['title'];
            $data['description'] = $inputArr['description'];
            $data['image'] = $inputArr['image'];
            $data['solution_type'] = $inputArr['solution_type'];
            if($this->db->update('solutions', $data)){
                return true;
            }else{
                return false;
            }
        }else{
            $page = str_replace('page-','',$inputArr['page']);
            $data['page_name'] = $inputArr['title'];
            $data['description'] = $inputArr['description'];
            $data['chart_data'] = $inputArr['chart_data'];
            $data['solution_id'] = $inputArr['solution_id'];
            $data['page_id'] = $page;

            if($this->db->update('solution_pages', $data)){
                return true;
            }else{
                return false;
            }
        }
    }
    public function getSingle($solution_id){
        $this->db->where('solution_id', $solution_id);
        $result = $this->db->get('solutions');
        $result = $result->result_array();

        $this->db->where('solution_id', $solution_id);
        $listResult = $this->db->get('solution_pages');
        $listResult = $listResult->result();
        $result = $result[0];
        $result['list'] = $listResult;
        return $result;
    }
    public function getPage($solutionId, $pageId){
        $this->db->where('solution_id', $solutionId);
        $this->db->where('page_id', $pageId);
        $result = $this->db->get('solution_pages');
        $result = $result->result_array();
        return $result;
    }
    public function getVariables($solutionId, $pageId){
        $this->db->where('solution_id', $solutionId);
        $this->db->where('page_id', $pageId);
        $this->db->select('chart_data, graph_type');
        $result = $this->db->get('solution_pages');
        return $result->result();
    }
    public function getSolutions($solutionType){
        $this->db->where('solution_type', $solutionType);
        $result = $this->db->get('solutions');
        $result = $result->result_array();

        return $result;
    }
}
