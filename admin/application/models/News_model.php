<?php

class News_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }
    public function create($arrInput){
        if($this->db->insert('news', $arrInput)){
            return true;
        }else{
            return false;
        }
    }
    public function get(){
        $this->db->order_by('date_published', 'DESC');
        $results = $this->db->get('news');
        return $results->result();
    }
    public function get_current(){
        $this->db->like('date_published', date('Y-m'), 'after');
        $this->db->order_by('news_id', 'DESC');
        $results = $this->db->get('news');
        return $results->result();
    }
    public function get_monthly($year){
        $arr = array();
        for($i = 1; $i <= 12; $i++){
            $month = $i < 10 ? '0'.$i : $i;
            $this->db->like('date_published', $year.'-'.$month, 'after');
            $results = $this->db->get('news');
            array_push($arr, $results->num_rows());
        }
        return $arr;

    }
    public function get_single($news_id){
        $this->db->where('news_id', $news_id);
        $this->db->order_by('date_published', 'DESC');
        $results = $this->db->get('news');
        return $results->result();
    }
    public function get_news_by_date($month, $year, $day){
        $months = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];
        $month = array_search($month, $months) + 1;
        $month = $month <= 9 ? "0".$month : $month;
        if($day == 0){
            $day = '';
        }else{
            $day = $day <= 9 ? "0".$day : $day;
        }
        $date = $year.'-'.$month.'-'.$day;
        $this->db->like('date_published', $date, 'after');
        $this->db->order_by('date_published', 'DESC');
        $results = $this->db->get('news');
        return $results->result();
    }
    public function get_next($news_id){
        $this->db->where('news_id > '.$news_id);
        $this->db->order_by('news_id' , 'ASC');
        $this->db->limit(1);
        $this->db->select('news_id');
        $results = $this->db->get('news');
        return $results->result();
    }
    public function get_prev($news_id){
        $this->db->where('news_id < '.$news_id);
        $this->db->order_by('news_id' , 'DESC');
        $this->db->limit(1);
        $this->db->select('news_id');
        $results = $this->db->get('news');
        return $results->result();
    }
    public function get_news_by_date_format($date){
        $this->db->like('date_published', $date, 'after');
        $this->db->order_by('news_id', 'DESC');
        $results = $this->db->get('news');
        return $results->result();
    }
    public function update_news($id, $title, $content, $thumb){
        $this->db->set('title', $title);
        $this->db->set('content', $content);
        $this->db->set('thumbnail', $thumb);
        $this->db->where('news_id', $id);
        if($this->db->update('news')){
            return true;
        }else{
            return false;
        }
    }
    public function save_preview($title, $content, $thumbnail, $date, $status){
        $this->db->set('title', $title);
        $this->db->set('content', $content);
        if($thumbnail != ''){
            $this->db->set('thumbnail', $thumbnail);
        }
        $this->db->set('date_published', $date);
        $this->db->set('status', $status);
        if($this->db->insert('news')){
            return $this->db->insert_id();
        }else{
            return 0;
        }

    }
    public function delete($news_id = ''){
        if($news_id != ''){
            $this->db->where('news_id', $news_id);
            if($this->db->delete('news')){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}
