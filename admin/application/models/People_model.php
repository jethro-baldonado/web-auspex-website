<?php

class People_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }
    public function save_cover($inputArr){
        if($this->db->insert('persons',$inputArr)){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    public function save_page($inputArr){
        if($this->db->insert('person_pages',$inputArr)){
            return true;
        }else{
            return false;
        }
    }
    public function update_cover($id, $inputArr){
        $this->db->where('person_id', $id);
        $this->db->set($inputArr);
        if($this->db->update('persons')){
            return true;
        }else{
            return false;
        }
    }
    public function update_page($id, $inputArr){
        $this->db->where('people_page_id', $id);
        $this->db->set($inputArr);
        if($this->db->update('person_pages')){
            return true;
        }else{
            return false;
        }
    }
    public function delete($id){
        $this->db->where('person_id', $id);
        if($this->db->delete("persons")){
            return true;
        }else{
            return false;
        }
    }
    public function delete_page($id){
        $this->db->where('people_page_id', $id);
        if($this->db->delete("person_pages")){
            return true;
        }else{
            return false;
        }
    }
    public function getNames(){
        $this->db->order_by('name', 'ASC');
        $this->db->select('person_id, name');
        $results = $this->db->get('persons');
        return $results->result();
    }
    public function getEmployee($id){
        $this->db->where('person_id',$id);
        $results = $this->db->get('persons');
        return $results->result();
    }
    public function getEmployeePages($id){
        $this->db->where('person_id', $id);
        $results = $this->db->get('person_pages');
        return $results->result();
    }
    public function get_all_employees(){
        $results = $this->db->get('persons');
        return $results->result();
    }
}
