<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/Forms.php';

class Ethos extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('auth')){
            if(!$this->session->userdata('auth')){
                redirect('login');
            }
        }
    }
    private function navGen($menu){
        $nav = '<ul class="horizontal-nav">';
        $nav .= $menu == 'mission' ? '<li class="active"><a href="'. base_url() .'ethos/mission">Mission</a></li>' : '<li class=""><a href="'. base_url() .'ethos/mission">Mission</a></li>';
        $nav .= $menu == 'vision' ? '<li class="active"><a href="'. base_url() .'ethos/vision">Vision</a></li>' : '<li class="" ><a href="'. base_url() .'ethos/vision">Vision</a></li>';
        $nav .= $menu == 'strength' ? '<li class="active"><a href="'. base_url() .'ethos/strength">Strength</a></li>' : '<li class=""><a href="'. base_url() .'ethos/strength">Strength</a></li>';
        $nav .= $menu == 'values' ? '<li class="active"><a href="'. base_url() .'ethos/values">Values</a></li>' : '<li class="" ><a href="'. base_url() .'ethos/values">Values</a></li>';
        $nav .= '</ul>';
        return $nav;
    }
    public function index(){
        redirect('ethos/mission');
    }
    public function mission(){
        $this->load->model('pages_model');
        $curValues = $this->pages_model->getPage(1);

        $options = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Title Page',
                'id' => 'title',
                'type' => 1,
                'value' => $curValues['title']

            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content (Max 300 Characters)',
                'id' => 'content',
                'type' => 1,
                'value' => $curValues['content']

            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb',
                'type' => 1,
                'value' => $curValues['background']

            ),
        );
        $form = new Forms($options);
        $form = $form->renderForm();
        $this->load->view('templates/header');

        $content = array(
            "nav"=> $this->navGen('mission'),
            "content" => $this->load->view('ethos/main',array(
                'form'=>$form,
                'submitLink' => 'ethos/save_mission'
            ),true),
            'page' => 'ethos',
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function vision(){
        $this->load->model('pages_model');
        $curValues = $this->pages_model->getPage(2);

        $options = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Title Page',
                'id' => 'title',
                'type' => 1,
                'value' => $curValues['title']

            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content (Max 300 Characters)',
                'id' => 'content',
                'type' => 1,
                'value' => $curValues['content']

            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb',
                'type' => 1,
                'value' => $curValues['background']

            ),
        );
        $form = new Forms($options);
        $form = $form->renderForm();
        $this->load->view('templates/header');

        $content = array(
            "nav"=> $this->navGen('vision'),
            "content" => $this->load->view('ethos/main',array(
                'form'=>$form,
                'submitLink' => 'ethos/save_vision'
            ),true),
            'page' => 'ethos'

        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function strength(){
        $this->load->model('pages_model');
        $curValues = $this->pages_model->getPage(3);
        $options = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Title Page',
                'id' => 'title',
                'type' => 1,
                'value' => $curValues['title']

            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content (Max 300 Characters)',
                'id' => 'content',
                'type' => 1,
                'value' => $curValues['content']

            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb',
                'type' => 1,
                'value' => $curValues['background']

            ),
        );
        $form = new Forms($options);
        $form = $form->renderForm();
        $this->load->view('templates/header');

        $content = array(
            "nav"=> $this->navGen('strength'),
            "content" => $this->load->view('ethos/main',array(
                'form'=>$form,
                'submitLink' => 'ethos/save_strength'
            ),true),
            'page' => 'ethos'
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function values(){
        $this->load->model('pages_model');
        $curValues = $this->pages_model->getPage(4);
        $options = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Title Page',
                'id' => 'title',
                'type' => 1,
                'value' => $curValues['title']

            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content (Max 300 Characters)',
                'id' => 'content',
                'type' => 1,
                'value' => $curValues['content']

            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb',
                'type' => 1,
                'value' => $curValues['background']

            ),
        );
        $form = new Forms($options);
        $form = $form->renderForm();
        $this->load->view('templates/header');

        $content = array(
            "nav"=> $this->navGen('values'),
            "content" => $this->load->view('ethos/main',array(
                'form'=>$form,
                'submitLink' => 'ethos/save_values'
            ),true),
            'page' => 'ethos',

        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }

    /*
    * Saving Functions
    */
    public function save_mission(){
        $missionInput = $this->input->post();
        $missionInput['menu_id'] = 1;
        $this->fnSave($missionInput);
    }
    public function save_vision(){
        $visionInput = $this->input->post();
        $visionInput['menu_id'] = 2;
        $this->fnSave($visionInput);
    }
    public function save_strength(){
        $strengthInput = $this->input->post();
        $strengthInput['menu_id'] = 3;
        $this->fnSave($strengthInput);
    }
    public function save_values(){
        $valuesInput = $this->input->post();
        $valuesInput['menu_id'] = 4;
        $this->fnSave($valuesInput);
    }
    private function fnSave($input){
        $this->load->model('pages_model');
        $type = $this->input->post('type');
        $config['upload_path']          = APPPATH . '../assets/images/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 8192;
        $this->load->library('upload', $config);
        $uploadPath = '';
        if ( ! $this->upload->do_upload('image-thumb'))
        {
            if($this->input->post('file-upload-label') != ''){
                $input['background'] = $this->input->post('file-upload-label');
                unset($input['file-upload-label']);
                if($this->pages_model->save($input)){
                    $msg = array('status', array(
                        'status' => true,
                        'message' => 'Successfully updated page!'
                    ));
                }
            }
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            $uploadPath = '/assets/images/uploads/'.$data['upload_data']['file_name'];
            $input['background'] = $uploadPath;
            unset($input['file-upload-label']);
            if($this->pages_model->save($input)){
                $msg = array('status', array(
                    'status' => true,
                    'message' => 'Successfully updated page!'
                ));
            }
        }
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }
}
