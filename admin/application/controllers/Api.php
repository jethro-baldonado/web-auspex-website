<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    public function __construct(){
        parent::__construct();
    }
    public function get_current_news(){
        $this->load->model('news_model');
        $results = $this->news_model->get_current();
        if(count($results)){
            $resultArr = array();
            foreach($results as $result){
                array_push($resultArr, array(
                    'id' => $result->news_id,
                    'title' => $result->title,
                    'content' => strip_tags($result->content),
                    'datePublished' => $result->date_published,
                    'thumb' => base_url().'/'.$result->thumbnail,
                    'isArchive' => date('m', strtotime($result->date_published)) != date('m'),
                    'yearMonth' => date('Y-m', strtotime($result->date_published)),
                    'permalink' => str_replace(' ','-',strtolower($result->title)).$result->news_id
                ));
            }
            echo json_encode($resultArr);
        }
    }
    public function get_archive_count(){
        $this->load->model('news_model');
        $results = $this->news_model->get_monthly(date('Y'));
        echo json_encode($results);
    }
    public function get_archive_list($year='', $month=''){
        $this->load->model('news_model');
        $results = $this->news_model->get_news_by_date_format($year.'-'.$month);
        if(count($results)){
            $resultArr = array();
            foreach($results as $result){
                array_push($resultArr, array(
                    'id' => $result->news_id,
                    'title' => $result->title,
                    'content' => strip_tags($result->content),
                    'datePublished' => $result->date_published,
                    'thumb' => base_url().'/'.$result->thumbnail,
                    'isArchive' => date('m', strtotime($result->date_published)) != date('m'),
                    'yearMonth' => date('Y-m', strtotime($result->date_published)),
                    'permalink' => str_replace(' ','-',strtolower($result->title)).$result->news_id
                ));
            }
            echo json_encode($resultArr);
        }
    }
    public function get_news(){
        $this->load->model('news_model');
        $results = $this->news_model->get();
        if(count($results)){
            $resultArr = array();
            foreach($results as $result){
                array_push($resultArr, array(
                    'id' => $result->news_id,
                    'title' => $result->title,
                    'content' => strip_tags($result->content),
                    'datePublished' => $result->date_published,
                    'thumb' => base_url().'/'.$result->thumbnail,
                    'isArchive' => date('m', strtotime($result->date_published)) != date('m'),
                    'yearMonth' => date('Y-m', strtotime($result->date_published)),
                    'permalink' => str_replace(' ','-',strtolower($result->title)).$result->news_id
                ));
            }
            echo json_encode($resultArr);
        }

    }
    public function get_news_all(){
        $this->load->model('news_model');
        $date = date('Y-m-d');
        $results = $this->news_model->get_news_by_date_format($date);
        if(count($results)){
            $resultArr = array();
            foreach($results as $result){
                array_push($resultArr, array(
                    'id' => $result->news_id,
                    'title' => $result->title,
                    'content' => strip_tags($result->content),
                    'datePublished' => $result->date_published,
                    'thumb' => base_url().'/'.$result->thumbnail,
                    'isArchive' => date('m', strtotime($result->date_published)) != date('m'),
                    'yearMonth' => date('Y-m', strtotime($result->date_published)),
                    'permalink' => str_replace(' ','-',strtolower($result->title)).$result->news_id
                ));
            }
            echo json_encode($resultArr);
        }
    }
    public function get_single_news($news_id){
        $this->load->model('news_model');
        $results = $this->news_model->get_single($news_id);
        if(count($results)){
            $resultArr = array();
            foreach($results as $result){
                $next = $this->news_model->get_next($news_id);
                $prev = $this->news_model->get_prev($news_id);
                if(count($next)){
                    $next = $next[0]->news_id;
                }else{
                    $next = '';
                }
                if(count($prev)){
                    $prev = $prev[0]->news_id;
                }else{
                    $prev = '';
                }

                array_push($resultArr, array(
                    'id' => $result->news_id,
                    'title' => $result->title,
                    'content' => $result->content,
                    'datePublished' => $result->date_published,
                    'thumb' => base_url().'/'.$result->thumbnail,
                    'isArchive' => date('m', strtotime($result->date_published)) != date('m'),
                    'yearMonth' => date('Y-m', strtotime($result->date_published)),
                    'permalink' => str_replace(' ','-',strtolower($result->title)).$result->news_id,
                    'year' => date('Y', strtotime($result->date_published)),
                    'month' => date('M', strtotime($result->date_published)),
                    'day' => date('d', strtotime($result->date_published)),
                    'next' => $next,
                    'prev' => $prev
                ));
            }
            echo json_encode($resultArr);
        }
    }
    public function get_employees(){
        $this->load->model('people_model');
        $results = $this->people_model->get_all_employees();
        if(count($results)){
            $resultArr = array();
            foreach($results as $result){
                $contents = $this->people_model->getEmployeePages($result->person_id);
                $contentArr = array();
                $i = 1;
                foreach($contents as $content){
                    array_push($contentArr, array(
                        'page' => $i,
                        'title' => $content->title,
                        'content' => $content->description,
                        'background' => base_url() . '/' . $content->background
                    ));
                    $i++;
                }
                array_push($resultArr, array(
                    'id' => $result->person_id,
                    'name' => $result->name,
                    'position' => $result->position,
                    'description' => $result->description,
                    'background' => base_url() . '/' . $result->background,
                    'content' => $contentArr
                ));
            }
            echo json_encode($resultArr);
        }
    }
    public function get_single_emp($id){
        $this->load->model('people_model');
        $results = $this->people_model->getEmployee($id);
        if(count($results)){
            $resultArr = array();
            foreach($results as $result){
                $contents = $this->people_model->getEmployeePages($id);
                $contentArr = array();
                $i = 1;
                foreach($contents as $content){
                    array_push($contentArr, array(
                        'page' => $i,
                        'title' => $content->title,
                        'content' => $content->description,
                        'background' => base_url() . '/' . $content->background
                    ));
                    $i++;
                }
                array_push($resultArr, array(
                    'id' => $result->person_id,
                    'name' => $result->name,
                    'position' => $result->position,
                    'description' => $result->description,
                    'background' => base_url() . '/' . $result->background,
                    'content' => $contentArr
                ));
            }
            echo json_encode($resultArr[0]);
        }
    }
    public function get_ethos(){
        $this->load->model('pages_model');
        $pageInfo = $this->pages_model->getAllPages();
        $result = array();
        foreach($pageInfo as $pages){
            array_push($result, array(
                    'title' => $pages['title'],
                    'content' => $pages['content'],
                    'background' => base_url($pages['background']),
                    'menu_id' => $pages['menu_id']
                )
            );
        }
        echo json_encode($result);
    }
    public function get_global(){
        $this->load->model('pages_model');
        $pages = $this->pages_model->getPage(5);
        $result = array(
                'title' => $pages['title'],
                'content' => $pages['content'],
                'background' => base_url($pages['background']),
                'menu_id' => $pages['menu_id']
            );

        echo json_encode($result);
    }

    public function get_solutions($type){
        $solutionType = 1;
        switch($type){
            case 'web':
                $solutionType = 1;
            break;
            case 'mobile':
                $solutionType = 2;
            break;
            case 'others':
                $solutionType = 3;
            break;
        }
        $this->load->model('solutions_model');
        $solutions = $this->solutions_model->getSolutions($solutionType);
        echo json_encode($solutions);
    }
    public function get_single_solution($id){
        $this->load->model('solutions_model');
        $solution = $this->solutions_model->getSingle($id);
        echo json_encode($solution);
    }
}
