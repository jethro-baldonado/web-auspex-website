<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/Forms.php';

class Archives extends CI_Controller{

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('auth')){
            if(!$this->session->userdata('auth')){
                redirect('login');
            }
        }
    }
    public function _remap($param){
        $this->index($param);
    }
    private function navGen($menu, $year = "", $day = ""){
        $months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        $nav = '<ul class="horizontal-nav date-nav col s9">';
        for($i = 0; $i < count($months); $i++){
            $nav .= $menu == substr(strtolower($months[$i]),0,3) ?
            '<li class="active"><a href="'. base_url() .'archives/'.substr(strtolower($months[$i]),0,3).'">'.substr($months[$i],0,3).'</a></li>' :
            '<li class=""><a href="'. base_url() .'archives/'.substr(strtolower($months[$i]),0,3).'">'.substr($months[$i],0,3).'</a></li>';
        }
        $nav .= '</ul>';
        $nav .= '<div class="date-container col s1 input-field">';

        $days = 1;
        switch($menu){
            case 'jan':
            case 'mar' :
            case 'may' :
            case 'july' :
            case 'aug' :
            $days = 31;
            break;
            case 'apr':
            case 'jun':
            case 'sep' :
            case 'nov' :
            $days = 30;
            break;
            default:
            $days = $year % 4 == 0 ? 29 : 28;
            break;
        }

        $nav .= '<select id="arch-day">';
        $nav .= '<option disabled>Day</option>';
        $nav .= '<option value="all" selected>All</option>';
        for($i = 1; $i <= $days; $i++){
            if($i == $day){
                $nav .= '<option value="'.$i.'" selected>'.$i.'</option>';
            }else{
                $nav .= '<option value="'.$i.'">'.$i.'</option>';
            }
        }
        $nav.='</select>';
        $nav .= '</div>';
        $nav .= '<div class="date-container col s1 input-field">';
        $nav .= '<select id="arch-year">';
        $nav .= "<option disabled>Year</option>";
        $nav .= '<option value="2017">2017</option>';
        $nav .= '</select>';

        $nav .= '</div>';
        $nav .= '<div class="date-container col s1" style="padding-left:20px;">';
        $nav .= '<a href="" class="center-align">DEACTIVATE</a>';
        $nav .= '</div>';
        return $nav;
    }
    public function index($month="", $year="", $day=""){
        echo $year;

        $this->load->model('news_model');
        if($month == "index"){
            $month = "jan";
            $year = "2017";
            $day = "All";
        }else{
            $month = $this->uri->segment(2);
            $year = $this->uri->segment(3);
            $day = $this->uri->segment(4);
        }
        $results = $this->news_model->get_news_by_date($month, $year, $day);
        $formOptions = array();
        if(count($results)){
            foreach($results as $result){
                array_push($formOptions, array(
                    'news_id' => $result->news_id,
                    'date_published' => $result->date_published,
                    'news_content' => array(
                        array(
                            'formType' => 'text',
                            'name' => 'news-title',
                            'label' => 'News Title',
                            'id' => 'news-title',
                            'type' => 1,
                            'value' => $result->title
                        ),
                        array(
                            'formType' => 'textarea',
                            'name' => 'content',
                            'label' => 'Content',
                            'id' => 'content',
                            'type' => 1,
                            'value' => $result->content
                        ),
                        array(
                            'formType' => 'upload',
                            'name' => 'image-thumb',
                            'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                            'id' => 'image-thumb',
                            'type' => 1,
                            'value' => $result->thumbnail
                        )
                    )
                ));
            }
        }
        $month2 = $month;
        $months = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"];
        $month2 = array_search($month2, $months) + 1;
        $month2 = $month2 <= 9 ? "0".$month2 : $month2;
        $day = $day <= 9 ? "0".$day : $day;
        $date = $year.'-'.$month2.'-'.$day;

        $form = new Forms($formOptions);
        $formData = $form->createSegments($date);
        $this->load->view('templates/header');
        $content = array(
            "nav"=> $this->navGen($month, $year, $day),
            "content" => $this->load->view('archive/archive',array('form'=>$formData, 'date' => $date),true),
            'page' => 'archive',
            'current_month' => $month
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');

    }

}
