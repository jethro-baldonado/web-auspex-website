<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/Forms.php';

class Globalpresence extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('auth')){
            if(!$this->session->userdata('auth')){
                redirect('login');
            }
        }
    }
    public function index(){
        $this->load->model('pages_model');
        $curValues = $this->pages_model->getPage(5);
        $options = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Title Page',
                'id' => 'title',
                'type' => 1,
                'value' => $curValues['title']

            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content (Max 300 Characters)',
                'id' => 'content',
                'type' => 1,
                'value' => $curValues['content']

            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb',
                'type' => 1,
                'value' => $curValues['background']

            ),
        );
        $form = new Forms($options);
        $form = $form->renderForm();
        $this->load->view('templates/header');

        $content = array(
            'nav' => '<a href="">Global Presence</a>',
            "content" => $this->load->view('ethos/main',array(
                'form'=>$form,
                'submitLink' => 'globalpresence/save_global'
            ),true),
            'page' => 'globalpresence',
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function save_global(){
        $globalInput = $this->input->post();
        $globalInput['menu_id'] = 5;
        $this->fnSave($globalInput);
    }
    private function fnSave($input){
        $this->load->model('pages_model');
        $type = $this->input->post('type');
        $config['upload_path']          = APPPATH . '../assets/images/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 8192;
        $this->load->library('upload', $config);
        $uploadPath = '';
        if ( ! $this->upload->do_upload('image-thumb'))
        {
            if($this->input->post('file-upload-label') != ''){
                $input['background'] = $this->input->post('file-upload-label');
                unset($input['file-upload-label']);
                if($this->pages_model->save($input)){
                    $msg = array('status', array(
                        'status' => true,
                        'message' => 'Successfully updated page!'
                    ));
                }
            }
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            $uploadPath = '/assets/images/uploads/'.$data['upload_data']['file_name'];
            $input['background'] = $uploadPath;
            unset($input['file-upload-label']);
            if($this->pages_model->save($input)){
                $msg = array('status', array(
                    'status' => true,
                    'message' => 'Successfully updated page!'
                ));
            }
            echo json_encode($msg);
        }
        $this->load->library('user_agent');
        redirect($this->agent->referrer());
    }
}
