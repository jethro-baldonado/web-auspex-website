<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

        if($this->session->has_userdata('auth')){
            if($this->session->userdata('auth')){
                redirect('news');
            }
		}
        $this->load->view('templates/header');
	    $this->load->view('login/login');
        $this->load->view('templates/footer');
	}
	public function auth(){
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$this->load->model('users_model');
		$userinfo = $this->users_model->login($email, $password);
		if(count($userinfo)){

			$this->session->set_userdata('auth', true);
			$this->session->set_userdata('userinfo', $userinfo);
			redirect('news');
		}else{
			$this->session->set_flashdata('message', 'Username or password is invalid!');
			redirect('/');
		}
	}
	public function logout(){
		$this->session->unset_userdata('auth');
		$this->session->unset_userdata('userinfo');
		redirect('/');
	}
}
