<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/Forms.php';

class News extends CI_Controller {

    /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    * 		http://example.com/index.php/welcome
    *	- or -
    * 		http://example.com/index.php/welcome/index
    *	- or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see https://codeigniter.com/user_guide/general/urls.html
    */
    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('auth')){
            if(!$this->session->userdata('auth')){
                redirect('login');
            }
        }

    }
    private function navGen($menu){
        $nav = '<ul class="horizontal-nav">';
        $nav .= $menu == 'cover' ? '<li class="active"><a href="'. base_url() .'news/cover">Cover</a></li>' : '<li class=""><a href="'. base_url() .'news/cover">Cover</a></li>';
        $nav .= $menu == 'recent' ? '<li class="active"><a href="'. base_url() .'news">Recent</a></li>' : '<li class="" ><a href="'. base_url() .'news">Recent</a></li>';
        $nav .= '</ul>';
        return $nav;
    }
    public function index()
    {
        $options = array(
            array(
                'formType' => 'text',
                'name' => 'news-title',
                'label' => 'News Title',
                'id' => 'news-title',
                'type' => 1
            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content',
                'id' => 'content',
                'type' => 1
            ),
            array(
                'formType' => 'date',
                'name' => 'date-publish',
                'label' => 'Date',
                'id' => 'date-publish',
                'type' => 1
            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb',
                'type' => 1
            ),
        );
        $form = new Forms($options);
        $form = $form->renderForm();
        $this->load->view('templates/header');

        $content = array(
            "nav"=> $this->navGen('recent'),
            "content" => $this->load->view('news/recent',array('form'=>$form),true),
            'page' => 'news'
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function cover(){
        $formArr = array(
            array(
                'formType' => 'upload',
                'name' => 'bg-image',
                'label' => 'Background Image( 300 DPI, 3000px x 3000px)',
                'id' => 'bg-image',
                'type' => 1
            )
        );
        $form = new Forms($formArr);
        $form = $form->renderForm();
        $this->load->view('templates/header');

        $content = array(
            'nav' => $this->navGen('cover'),
            'content' => $this->load->view('news/cover', array('form' => $form), true),
            'page' => 'news'
        );
        $this->load->view('templates/content', $content);
        $this->load->view('templates/footer');
    }
    public function create(){
        $title = $this->input->post('news-title');
        $content = $this->input->post('content');
        $date = $this->input->post('date-publish-year').'-'.sprintf('%02s', $this->input->post('date-publish-month')).'-'.sprintf('%02s', $this->input->post('date-publish-day'));
        $config['upload_path']          = './assets/images/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 4096;
        $this->load->library('upload', $config);
        $uploadPath = '';
        if ( ! $this->upload->do_upload('image-thumb'))
        {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('status', array(
                    'status' => false,
                    'message' => 'Error adding news!'
                ));
        }
        else
        {
                $data = array('upload_data' => $this->upload->data());
                $uploadPath = '/assets/images/uploads/'.$data['upload_data']['file_name'];
                $this->load->model('news_model');
                $inputArr = array(
                    'title' => $title,
                    'content' => $content,
                    'thumbnail' => $uploadPath,
                    'date_published' => $date.' '.date('H:i:s'),
                    'status' => 1
                );
                if($this->news_model->create($inputArr)){
                    $this->session->set_flashdata('status', array(
                        'status' => true,
                        'message' => 'Successfully added news!'
                    ));
                }else{
                    $this->session->set_flashdata('status', array(
                        'status' => false,
                        'message' => 'Error adding news!'
                    ));
                }
        }
        redirect('/news');
    }
    public function upload(){
        $config['upload_path'] = './assets/images/backgrounds';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 4096;
        $this->load->library('upload', $config);
        $uploadPath = '';
        if(!$this->upload->do_upload('bg-image')){
            $error = array('error' => $this->upload->display_errors());
        }else{
            $data = array('upload_data' => $this->upload->data());
            $uploadPath = '/assets/images/backgrounds' . $data['upload_data']['file_name'];
            $this->load->model('Pages_model');
            $inputArr = array(
                'bg_path' => $uploadPath
            );
            if($this->pages_model->updateBg($inputArr)){

            }else{

            }
        }
    }
    public function update(){
        $config['upload_path'] = './assets/images/uploads';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = 4096;
        $this->load->library('upload', $config);

        $this->load->model('news_model');
        if($this->input->post('image-thumb') != 'undefined' && $this->input->post('file-upload-label') != ''){
            if ( ! $this->upload->do_upload('image-thumb'))
            {
                    $error = array('error' => $this->upload->display_errors());
                    $this->session->set_flashdata('status', array(
                        'status' => false,
                        'message' => 'Error uploading Image!'
                    ));
            }else{
                $data = array('upload_data' => $this->upload->data());
                $thumbPath = '/assets/images/uploads/' . $data['upload_data']['file_name'];

                $inputArr = array(
                    'title' => $this->input->post('news-title'),
                    'content' => $this->input->post('content'),
                    'thumbnail' => $thumbPath
                );
                if( $this->news_model->update_news(
                        $this->input->post('news-id'),
                        $this->input->post('news-title'),
                        $this->input->post('content'),
                        $thumbPath
                    )
                ){
                    $data = array(
                        'status' => true,
                        'message' => 'Successfully updated news!'
                    );
                }else{
                    $data = array(
                        'status' => false,
                        'message' => 'Error updating news!'
                    );
                }
                echo json_encode($data);
            }
        }else{
            $inputArr = array(
                'title' => $this->input->post('news-title'),
                'content' => $this->input->post('content'),
                'thumbnail' => $this->input->post('file-upload-label')
            );
            if( $this->news_model->update_news(
                    $this->input->post('news-id'),
                    $this->input->post('news-title'),
                    $this->input->post('content'),
                    $this->input->post('file-upload-label')
                )
            ){
                $data = array(
                    'status' => true,
                    'message' => 'Successfully updated news!'
                );
            }else{
                $data = array(
                    'status' => false,
                    'message' => 'Error updating news!'
                );
            }
            echo json_encode($data);
        }
    }
    public function save_preview(){
        $newsTitle = $this->input->post('news-title');
        $content = $this->input->post('content');


        $thumbnail = $this->input->post('thumb');

        $date = $this->input->post('date');

        $this->load->model('news_model');
        $config['upload_path']          = './assets/images/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 4096;
        $this->load->library('upload', $config);
        $uploadPath = '';

        if($this->input->post('image-thumb') != 'undefined' && $this->input->post('file-upload-label') != ''){
            if ( ! $this->upload->do_upload('image-thumb'))
            {
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
            }
            else
            {
                $data = array('upload_data' => $this->upload->data());
                $uploadPath = '/assets/images/uploads/'.$data['upload_data']['file_name'];
                $last_id = $this->news_model->save_preview($newsTitle, $content, $uploadPath, $date, 0);
                if($last_id){
                    echo $last_id;
                }else{
                    echo "Error";
                }
            }
        }else{
            $last_id = $this->news_model->save_preview($newsTitle, $content, $this->input->post('file-upload-label'), $date, 0);
            if($last_id){
                echo $last_id;
            }else{
                echo "Error";
            }
        }

    }
    public function preview($news_id=""){
        if($news_id == ""){
            redirect($_SERVER['HTTP_REFERER']);
        }
        $this->load->model('news_model');
        $results = $this->news_model->get_single($news_id);

        $this->load->view('news/preview', $results[0]);
    }
    public function preview_update(){
        $this->load->model('news_model');

    }
    public function delete($id = ""){
        if($id != ''){
            $this->load->model('news_model');
            if($this->news_model->delete($id)){
                $data = array(
                    'msg' => 'Successfully deleted news!',
                    'status' => true
                );
            }else{
                $data = array(
                    'msg' => 'Error deleting news.',
                    'status' => false
                );
            }
        }else{
            $data = array(
                'msg' => 'No news selected.',
                'status' => false
            );
        }
        echo json_encode($data);
    }
}
