<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/Forms.php';

class Solutions extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('auth')){
            if(!$this->session->userdata('auth')){
                redirect('login');
            }
        }
    }
    private function navGen($menu, $pId = ""){

        $this->load->model('solutions_model');
        $solutions = $this->solutions_model->getSolutionList();
        $nav = '<ul class="horizontal-nav person-nav solution-nav col s9">';
        $nav .= $menu == 'cover' ? '<li class="active"><a href="'. base_url() .'solutions/">Cover</a></li>' : '<li class=""><a href="'. base_url() .'people">Cover</a></li>';
        $nav .= $menu == 'page-1' ? '<li class="active"><a href="'. base_url() .'solutions/create/1">Page 1</a></li>' : '<li class="" ><a href="'. base_url() .'people/create/1">Page 1</a></li>';
        $nav .= $menu == 'page-2' ? '<li class="active"><a href="'. base_url() .'solutions/create/2">Page 2</a></li>' : '<li class=""><a href="'. base_url() .'people/create/2">Page 2</a></li>';
        $nav .= $menu == 'page-3' ? '<li class="active"><a href="'. base_url() .'solutions/create/3">Page 3</a></li>' : '<li class="" ><a href="'. base_url() .'people/create/3">Page 3</a></li>';
        $nav .= '</ul>';

        if(count($solutions)){
            $nav .= '<div class="date-container col s3 input-field" style="width:24%;">';
            $nav .= '<select id="solution-list">';
            $nav .= '<option value="0">Add Solution</option>';
            foreach($solutions as $solution){
                if($pId == $solution->solution_id){
                    $nav .= '<option value="'.$solution->solution_id.'" selected="selected">' . $solution->title .'</option>';

                }else{
                    $nav .= '<option value="'.$solution->solution_id.'">' . $solution->title .'</option>';

                }
            }
            $nav.='</select>';
            $nav .= '</div>';
        }
        return $nav;
    }
    public function index(){
        redirect('solutions/web');
    }
    public function web($page = ""){
        $form = "";
        $this->load->view('templates/header');
        $content = array(
            'nav' => $this->navGen('cover', $page),

            "content" => $this->load->view('solutions/solutions_container',array(
                'form'=>$form,
                'submitLink' => 'solutions/save_solution',
                'lastId' => $page,
            ),true),
            'page' => 'web',
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function mobile($page = ""){
        $form = "";
        $this->load->view('templates/header');
        $content = array(
            'nav' => $this->navGen('cover', $page),

            "content" => $this->load->view('solutions/solutions_container',array(
                'form'=>$form,
                'submitLink' => 'solutions/save_solution',
                'lastId' => $page,
            ),true),
            'page' => 'web',
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function other($page = ""){
        $form = "";
        $this->load->view('templates/header');
        $content = array(
            'nav' => $this->navGen('cover', $page),

            "content" => $this->load->view('solutions/solutions_container',array(
                'form'=>$form,
                'submitLink' => 'solutions/save_solution',
                'lastId' => $page,
            ),true),
            'page' => 'web',
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function render_form($page = "", $pageId = ""){
        if($pageId != ''){
            $this->load->model('solutions_model');
            if($page == 'cover')
                $result = $this->solutions_model->getSingle($pageId);
            else{
                $result = $this->solutions_model->getPage($pageId, str_replace('page%20', '',$page));
                if(!empty($result)){
                    if(is_array(json_decode($result[0]['chart_data']))){
                        $chart_data = json_decode($result[0]['chart_data']);
                    }
                }
            }
        }
        $options = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Description Title',
                'id' => 'title',
                'type' => 1,
                'value' => !empty($result) && $page == 'cover' ? $result['title'] : ''
            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content (Max 300 Characters)',
                'id' => 'content',
                'type' => 1,
                'value' => !empty($result) && $page == 'cover' ? $result['description'] : ''
            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Main Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb',
                'type' => 1,
                'value' => !empty($result) && $page == 'cover' ? $result['image'] : ''
            ),
            array(
                'formType' => 'colorSelect',
                'name' => 'colorOptions',
                'label' => 'Background Color',
                'id' => '',
                'type' => 1,
                'value' => !empty($result) && $page == 'cover' && $result['bg_color'] != '' ? 2 : ''
            )
        );
        $options2 = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Description Title',
                'id' => 'title',
                'type' => 1,
                'value' => !empty($result) && $page != 'cover' ? $result[0]['page_name'] : ''
            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Content (Max 300 Characters)',
                'id' => 'content',
                'type' => 1,
                'value' => !empty($result) && $page != 'cover' ? $result[0]['description'] : ''
            ),
            array(
                'formType' => 'solutionSelect',
                'name' => 'options',
                'label' => 'Content Type',
                'id' => '',
                'type' => 1,
                'value' => !empty($result) && $page != 'cover' && $result[0]['graph_type'] != '' ? 2 : ''
            )
        );

        if($page == "cover"){
            $form = new Forms($options);
            $form = $form->renderForm();
        }else{
            $form = new Forms($options2);
            if(!empty($result) && $page != 'cover' && $result[0]['graph_type'] != 'image'){
                $graphType = $result[0]['graph_type'];
                $graphTypeVal = 1;
                if($graphType == 'lines')
                $graphTypeVal = 1;
                else if($graphType == 'bars')
                $graphTypeVal = 2;
                else if($graphType == 'pie')
                $graphTypeVal = 3;

                $form = $form->renderForm($this->request_graph_format($graphTypeVal, '', str_replace('page%20', '',$page),$pageId));
            }else{
                $form = $form->renderForm();
            }
        }
        echo '<div class="col s7">';
        echo form_open_multipart('news/cover_upload',array('class'=>'form-builder',
            'data-type' => str_replace('%20','-',$page),
            'data-page-id' => $pageId
        ));
        echo $form;
        echo '<div class="row">';
        echo '<div class="col s6 center-align">
        <button type="button" class="form-btn">PREVIEW</button>
        </div>
        <div class="col s6 center-align">
        <button type="submit" class="form-btn" id="save-solution">SAVE</button>
        </div>
        </div>';
        echo form_close();
        echo '</div>';

    }
    public function request_graph_format($type,$rowId, $pageId = '', $solutionId = ''){
        $graph = '';

        if($solutionId != '' && $pageId != ''){
            $this->load->model('solutions_model');
            $result = $this->solutions_model->getPage($solutionId, $pageId);
            $result = json_decode($result[0]['chart_data']);
            if(json_last_error() != JSON_ERROR_NONE){
                for($i = 0; $i < count($result->labels); $i++){
                    $graph .= $this->graphType($type, $rowId, $result->labels[$i], $result->datasets[0]->backgroundColor[$i], $result->datasets[0]->data[$i], isset($variable->stroke) ? $variable->stroke : '', isset($variable->location) ? $variable->location : '');
                }
            }
        }else{
            echo $this->graphType($type, $rowId);
        }
        return $graph;
    }
    private function graphType($type, $rowId, $label = "", $color = "", $value = "", $stroke = "", $barPos = ""){
        switch($type){
            case 1:
            $graph = <<<EOT
            <div class="var-list-item-line list-item-$rowId">
            <div class="row var-head">
            <div class="col s8">Variable $rowId</div>
            <div class="col s3">
            <a href="">Reset</a> | <a href="">Delete</a>
            </div>
            <div class="col s1">
            <a href="" class="var-item-control"><i class="fa fa-chevron-down"></i></a>
            </div>
            </div>
            <div class="row var-content">
            <div class="col s6">
            <div class="row valign-wrapper">
            <div class="col s3"><label>Label</label></div>
            <div class="col s9"><input type="text" class="label" value="$label"></div>
            </div>
            <div class="row valign-wrapper">
            <div class="col s3"><label>Color</label></div>
            <div class="col s9"><input type="text" class="color" value="$color"></div>
            </div>
            </div>
            <div class="col s6">
            <div class="row valign-wrapper">
            <div class="col s3"><label>Value</label></div>
            <div class="col s9"><input type="text" class="value" value="$value"></div>
            </div>
            <div class="row valign-wrapper" style="padding-top: 10px;">
            <div class="col s3"><label>Stroke</label></div>
            <div class="col s9"><p class="range-field"><input type="range" id="stroke-range" min="0" max="10" value="$stroke"/></p></div>
            </div>
            </div>
            </div>
            </div>
EOT;
            break;
            case 2:
            $graph = <<<EOT
            <div class="var-list-item-bar list-item-$rowId">
            <div class="row var-head">
            <div class="col s8">Variable $rowId</div>
            <div class="col s3">
            <a href="">Reset</a> | <a href="">Delete</a>
            </div>
            <div class="col s1">
            <a href="" class="var-item-control"><i class="fa fa-chevron-down"></i></a>
            </div>
            </div>
            <div class="row var-content">
            <div class="col s6">
            <div class="row valign-wrapper">
            <div class="col s3"><label>Label</label></div>
            <div class="col s9"><input type="text" class="label" value="$label"></div>
            </div>
            <div class="row valign-wrapper">
            <div class="col s3"><label>Color</label></div>
            <div class="col s9"><input type="text" class="color" value="$color"></div>
            </div>
            </div>
            <div class="col s6">
            <div class="row valign-wrapper">
            <div class="col s3"><label>Value</label></div>
            <div class="col s9"><input type="text" class="value" value="$value"></div>
            </div>
            <div class="row valign-wrapper" style="padding-top: 20px;">
            <div class="col s6"><label>Label Position</label></div>
            <div class="col s6">
            <input type="checkbox" class="filled-in" id="filled-in-box-$rowId"/>
            <label for="filled-in-box-$rowId">Inside Box</label>
            </div>
            </div>
            </div>
            </div>
            </div>
EOT;
            break;
            case 3:
            $graph = <<<EOT
            <div class="var-list-item-pie list-item-$rowId">
            <div class="row var-head">
            <div class="col s8">Variable $rowId</div>
            <div class="col s3">
            <a href="">Reset</a> | <a href="">Delete</a>
            </div>
            <div class="col s1">
            <a href="" class="var-item-control"><i class="fa fa-chevron-down"></i></a>
            </div>
            </div>
            <div class="row var-content">
            <div class="col s6">
            <div class="row valign-wrapper">
            <div class="col s3"><label>Label</label></div>
            <div class="col s9"><input type="text" class="label" value="$label"></div>
            </div>
            <div class="row valign-wrapper">
            <div class="col s3"><label>Color</label></div>
            <div class="col s9"><input type="text" class="color" value="$color"></div>
            </div>
            </div>
            <div class="col s6">
            <div class="row valign-wrapper">
            <div class="col s3"><label>Value</label></div>
            <div class="col s9"><input type="text" class="value" value="$value"></div>
            </div>
            </div>
            </div>
            </div>
EOT;
            break;
        }
        return $graph;
    }
    public function create(){
        $this->load->model('solutions_model');
        $inputArr = array();

        $title = $this->input->post('title');
        $description = $this->input->post('content');
        $contentType = $this->input->post('content-type');
        $page = $this->input->post('type');
        $inputArr['title'] = $title;
        $inputArr['description'] = $description;
        $inputArr['page'] = $page;

        if($this->input->post('solution-id')){
            $inputArr['solution_id'] = $this->input->post('solution-id');
        }
        switch($this->input->post('solution-type')){
            case 'web' :
                $inputArr['solution_type'] = 1;
                break;
            case 'mobile' :
                $inputArr['solution_type'] = 2;
            break;
            case 'others' :
                $inputArr['solution_type'] = 3;
            break;
        }
        if($contentType == 1 || $contentType == 'undefined'){
            $config['upload_path']          = APPPATH . '../assets/images/solutions/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 8192;
            $this->load->library('upload', $config);
            $uploadPath = '';
            if ( ! $this->upload->do_upload('upload'))
            {
                $error = array('error' => $this->upload->display_errors());
                $msg = array(array(
                    'status' => false,
                    'message' => 'Error updating page!'
                ));
                echo json_encode($error);
            }
            else{
                $data = array('upload_data' => $this->upload->data());
                $uploadPath = '/assets/images/uploads/'.$data['upload_data']['file_name'];
                if($page == 'cover'){
                    $inputArr['image'] = $uploadPath;
                    $inputArr['bg_color'] = $this->input->post('bg_color');
                }else{
                    $inputArr['graph_type'] = 'image';
                    $inputArr['chart_data'] = $uploadPath;
                }
                $res = $this->solutions_model->save($inputArr);
                if($res > 0){
                    $msg = array(array(
                        'status' => true,
                        'message' => 'Successfully created solution!'
                    ));
                }
                if($page == 'cover'){
                    $msg = array(array(
                        'status' => true,
                        'message' => 'Successfully created solution!',
                        'redirectionUrl' => base_url('/solutions/' . $this->input->post('solution-type') .'/'.$res)
                    ));
                }
                echo json_encode($msg);
            }
        }else{
            $inputArr['graph_type'] = $this->input->post('graph-type');
            $chartContent = $this->input->post('chartData');
            $inputArr['chart_data'] = $chartContent;
            $res = $this->solutions_model->save($inputArr);
            if($res > 0){
                $msg = array(array(
                    'status' => true,
                    'message' => 'Successfully created solution!'
                ));
            }
            echo json_encode($msg);
        }
    }
    public function update(){
        $this->load->model('solutions_model');
        $this->solutions_model->update();

    }
}
