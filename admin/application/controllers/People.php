<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/Forms.php';

class People extends CI_Controller{

    public function __construct(){
        parent::__construct();
    }
    private function navGen($menu, $pId = ""){

        $this->load->model('people_model');
        $people = $this->people_model->getNames();

        $nav = '<ul class="horizontal-nav person-nav col s9">';
        $nav .= $menu == 'cover' ? '<li class="active"><a href="'. base_url() .'people/">Cover</a></li>' : '<li class=""><a href="'. base_url() .'people">Cover</a></li>';
        $nav .= $menu == 'page-1' ? '<li class="active"><a href="'. base_url() .'people/create/1">Page 1</a></li>' : '<li class="" ><a href="'. base_url() .'people/create/1">Page 1</a></li>';
        $nav .= $menu == 'page-2' ? '<li class="active"><a href="'. base_url() .'people/create/2">Page 2</a></li>' : '<li class=""><a href="'. base_url() .'people/create/2">Page 2</a></li>';
        $nav .= $menu == 'page-3' ? '<li class="active"><a href="'. base_url() .'people/create/3">Page 3</a></li>' : '<li class="" ><a href="'. base_url() .'people/create/3">Page 3</a></li>';
        $nav .= '</ul>';

        if(count($people)){
            $nav .= '<div class="date-container col s3 input-field" style="width:24%;">';
            $nav .= '<select id="person-list">';
            $nav .= '<option value="0">New Employee</option>';
            foreach($people as $person){
                if($pId == $person->person_id){
                    $nav .= '<option value="'.$person->person_id.'" selected="selected">' . $person->name .'</option>';

                }else{
                    $nav .= '<option value="'.$person->person_id.'">' . $person->name .'</option>';

                }
            }
            $nav.='</select>';
            $nav .= '</div>';
        }
        return $nav;
    }
    public function index(){
        redirect('people/create');
    }
    public function create($page = ""){
        $this->load->view('templates/header');
        $nav = $this->navGen('cover');
        $options = array(
            array(
                'formType' => 'text',
                'name' => 'employee-name',
                'label' => 'Employee Name',
                'id' => 'emp-name',
                'type' => 1
            ),
            array(
                'formType' => 'text',
                'name' => 'employee-position',
                'label' => 'Employee Position',
                'id' => 'emp-position',
                'type' => 1
            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Description',
                'id' => 'content-cover',
                'type' => 1
            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Background Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb-cover',
                'type' => 1
            ),
        );

        $options1 = array(

            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Page Title',
                'id' => 'title-1',
                'type' => 1
            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Description',
                'id' => 'content-1',
                'type' => 1
            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Background Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb-1',
                'type' => 1
            ),
        );

        $options2 = array(

            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Page Title',
                'id' => 'title-2',
                'type' => 1
            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Description',
                'id' => 'content-2',
                'type' => 1
            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Background Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb-2',
                'type' => 1
            ),
        );

        $options3 = array(
            array(
                'formType' => 'text',
                'name' => 'title',
                'label' => 'Page Title',
                'id' => 'title-3',
                'type' => 1
            ),
            array(
                'formType' => 'textarea',
                'name' => 'content',
                'label' => 'Description',
                'id' => 'content-3',
                'type' => 1
            ),
            array(
                'formType' => 'upload',
                'name' => 'image-thumb',
                'label' => 'Background Image (300 DPI, 3000px x 3000px)',
                'id' => 'image-thumb-3',
                'type' => 1
            ),
        );

        $form = new Forms($options);
        $form = $form->renderForm();
        $form1 = new Forms($options1);
        $form1 = $form1->renderForm();
        $form2 = new Forms($options2);
        $form2 = $form2->renderForm();
        $form3 = new Forms($options3);
        $form3 = $form3->renderForm();
        $content = array(
            "nav"=> $nav,
            "content" => $this->load->view('people/page1',array('form'=>$form, 'form1'=>$form1, 'form2'=>$form2, 'form3'=>$form3),true),
            'page' => 'cover'
        );
        $this->load->view('templates/content',$content);
        $this->load->view('templates/footer');
    }
    public function save(){
        $this->load->model('people_model');
        $type = $this->input->post('type');
        $config['upload_path']          = './assets/images/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 8192;
        $this->load->library('upload', $config);
        $uploadPath = '';
        if ( ! $this->upload->do_upload('image-thumb'))
        {
            $error = array('error' => $this->upload->display_errors());
            $msg = array('status', array(
                'status' => false,
                'message' => 'Error adding employee!'
            ));
            echo json_encode($error);
        }
        else{
            $data = array('upload_data' => $this->upload->data());
            $uploadPath = '/assets/images/uploads/'.$data['upload_data']['file_name'];
            if($type == '1'){
                $name = $this->input->post('employee-name');
                $position = $this->input->post('employee-position');
                $description = $this->input->post('content');
                $inputArr = array(
                    "name" => $name,
                    "description" => $description,
                    "background" => $uploadPath,
                    "menu_order" => 0,
                    "position" => $position
                );
                $empId = $this->people_model->save_cover($inputArr);
                if($empId){
                    $msg = array(
                        'status' => true,
                        'message' => 'Successfully created employee.',
                        'personId' => $empId
                    );
                }else{
                    $msg = array(
                        'status' => false,
                        'message' => 'Error creating employee'
                    );
                }
            }else{
                $empId = $this->input->post('id');
                $description = $this->input->post('content');
                $inputArr = array(
                    'person_id' => $empId,
                    'description' => $description,
                    'background' => $uploadPath
                );
                if($this->people_model->save_page($inputArr)){
                    $msg = array(
                        'status' => true,
                        'message' => 'Successfully added employee page.'
                    );
                }

            }
            echo json_encode($msg);
        }
    }
    public function update($id = ""){
        if($id != ""){
            $this->load->model('people_model');
            $person = $this->people_model->getEmployee($id);
            $personPages = $this->people_model->getEmployeePages($id);
            $personInfo = $person[0];

            $options = array(
                array(
                    'formType' => 'text',
                    'name' => 'employee-name',
                    'label' => 'Employee Name',
                    'id' => 'emp-name',
                    'type' => 1,
                    'value' => $personInfo->name
                ),
                array(
                    'formType' => 'text',
                    'name' => 'employee-position',
                    'label' => 'Employee Position',
                    'id' => 'emp-position',
                    'type' => 1,
                    'value' => $personInfo->position
                ),
                array(
                    'formType' => 'textarea',
                    'name' => 'content',
                    'label' => 'Description',
                    'id' => 'content-cover',
                    'type' => 1,
                    'value' => $personInfo->description
                ),
                array(
                    'formType' => 'upload',
                    'name' => 'image-thumb',
                    'label' => 'Background Image (300 DPI, 3000px x 3000px)',
                    'id' => 'image-thumb-cover',
                    'type' => 1,
                    'value' => $personInfo->background
                ),
                array(
                    'formType' => 'hidden',
                    'name' => 'person-id',
                    'id' => 'person-id',
                    'value' => $personInfo->person_id
                ),
            );
            $form = new Forms($options);
            $form = $form->renderForm();

            $formArr = array();
            for($i = 0; $i < 3; $i++){
                $p =  $i > (count($personPages) - 1) ? '' : $personPages[$i];
                $options1 = array(
                    array(
                        'formType' => 'text',
                        'name' => 'title',
                        'label' => 'Page Title',
                        'id' => 'title-'.($i + 1),
                        'type' => 1,
                        'value' => $p != '' ? $p->title : ''
                    ),
                    array(
                        'formType' => 'textarea',
                        'name' => 'content',
                        'label' => 'Description',
                        'id' => 'content-'.($i + 1),
                        'type' => 1,
                        'value' => $p != '' ? $p->description : ''
                    ),
                    array(
                        'formType' => 'upload',
                        'name' => 'image-thumb',
                        'label' => 'Background Image (300 DPI, 3000px x 3000px)',
                        'id' => 'image-thumb-'.($i + 1),
                        'type' => 1,
                        'value' => $p != '' ? $p->background : ''
                    ),
                    array(
                        'formType' => 'hidden',
                        'name' => 'people-page-id',
                        'id' => 'people-page-id-'.($i + 1),
                        'value' => $p != '' ? $p->people_page_id : ''
                    ),
                );
                $formDt = new Forms($options1);
                array_push($formArr, $formDt->renderForm());
            }
            $this->load->view('templates/header');
            $content = array(
                "nav"=> $this->navGen('cover', $id),
                "content" => $this->load->view('people/other-pages',array('form'=>$form, 'formArr' => $formArr),true),
                'page' => 'cover'
            );
            $this->load->view('templates/content',$content);
            $this->load->view('templates/footer');
        }
    }
    public function update_save(){
        $this->load->model('people_model');
        $type = $this->input->post('type');
        $config['upload_path']          = './assets/images/uploads/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 8192;
        $this->load->library('upload', $config);
        $uploadPath = '';

        if($this->input->post('image-thumb') != 'undefined' && $this->input->post('file-upload-label') != ''){
            if ( ! $this->upload->do_upload('image-thumb'))
            {
                $error = array('error' => $this->upload->display_errors());
                $msg = array('status', array(
                    'status' => false,
                    'message' => 'Error updating employee!' . $error['error']
                ));
                echo json_encode($error);
            }
            else{
                $data = array('upload_data' => $this->upload->data());
                $uploadPath = '/assets/images/uploads/'.$data['upload_data']['file_name'];
                if($type == '1'){
                    $name = $this->input->post('employee-name');
                    $position = $this->input->post('employee-position');
                    $description = $this->input->post('content');
                    $id = $this->input->post('person-id');
                    $inputArr = array(
                        "name" => $name,
                        "description" => $description,
                        "background" => $uploadPath,
                        "menu_order" => 0,
                        "position" => $position
                    );
                    $empId = $this->people_model->update_cover($id, $inputArr);
                    if($empId){
                        $msg = array(
                            'status' => true,
                            'message' => 'Successfully updated employee.',
                            'personId' => $empId
                        );
                    }else{
                        $msg = array(
                            'status' => false,
                            'message' => 'Error updating employee'
                        );
                    }
                }else{
                    $pageId = $this->input->post('people-page-id');
                    $description = $this->input->post('content');
                    $title = $this->input->post('title');
                    $empId = $this->input->post('empId');
                    $inputArr = array(
                        'person_id' => $empId,
                        'description' => $description,
                        'background' => $uploadPath,
                        'title' => $title
                    );
                    if($pageId){
                        if($this->people_model->update_page($pageId, $inputArr)){
                            $msg = array(
                                'status' => true,
                                'message' => 'Successfully updated employee page.'
                            );
                        }
                    }else{
                        if($this->people_model->save_page($inputArr)){
                            $msg = array(
                                'status' => true,
                                'message' => 'Successfully added employee page.'
                            );
                        }
                    }

                }
            }
        }else{
            $pageId = $this->input->post('people-page-id');
            $description = $this->input->post('content');
            $title = $this->input->post('title');
            $empId = $this->input->post('empId');
            if($type == '1'){
                $name = $this->input->post('employee-name');
                $position = $this->input->post('employee-position');
                $description = $this->input->post('content');
                $id = $this->input->post('person-id');
                $inputArr = array(
                    "name" => $name,
                    "description" => $description,
                    "menu_order" => 0,
                    "position" => $position
                );
                $empId = $this->people_model->update_cover($id, $inputArr);
                if($empId){
                    $msg = array(
                        'status' => true,
                        'message' => 'Successfully updated employee.',
                        'personId' => $empId
                    );
                }else{
                    $msg = array(
                        'status' => false,
                        'message' => 'Error updating employee'
                    );
                }
            }else{
                $pageId = $this->input->post('people-page-id');
                $description = $this->input->post('content');
                $title = $this->input->post('title');
                $empId = $this->input->post('empId');
                $inputArr = array(
                    'person_id' => $empId,
                    'description' => $description,
                    'title' => $title
                );
                if($pageId){
                    if($this->people_model->update_page($pageId, $inputArr)){
                        $msg = array(
                            'status' => true,
                            'message' => 'Successfully updated employee page.'
                        );
                    }
                }else{
                    if($this->people_model->save_page($inputArr)){
                        $msg = array(
                            'status' => true,
                            'message' => 'Successfully added employee page.'
                        );
                    }
                }

            }
        }
        echo json_encode($msg);
    }
    public function delete($id=""){
        if($id != ''){
            $this->load->model('people_model');
            if($this->people_model->delete($id)){
                $msg = array(
                    'status' => true,
                    'message' => 'Successfully deleted employee.'
                );
            }else{
                $msg = array(
                    'status' => false,
                    'message' => 'Error deleting employee.'
                );
            }
        }else{
            $msg = array(
                'status' => false,
                'message' => 'Please select an employee to delete.'
            );
        }
        echo json_encode($msg);
    }
    public function delete_page($id=""){
        if($id != ''){
            $this->load->model('people_model');
            if($this->people_model->delete_page($id)){
                $msg = array(
                    'status' => true,
                    'message' => 'Successfully deleted employee page.'
                );
            }else{
                $msg = array(
                    'status' => false,
                    'message' => 'Errpr deleting employee page.'
                );
            }
        }else{
            $msg = array(
                'status' => false,
                'message' => 'Please select an employee page to delete.'
            );
        }
        echo json_encode($msg);
    }
}
