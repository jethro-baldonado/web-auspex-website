<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Forms{
    private $formControls;
    private $options;

    /*
    * @params (all elements is required)
    * options consists of the following for
    * each element of the array
    *   formType (text, textarea, date, upload, slider)
    *   name (any)
    *   label (any)
    *   id  (any)
    *   type (1 = label above form element : 2 = label inline with element)
    */
    public function __construct($options){
        $this->options = $options;
    }
    private function createSingleText($name, $label,$id, $type, $value=""){
        $control = "";
        $control .= '<div class="row">';
        $control .= '<div class="col s12">';
        $control .= $type == 1 ? '<label class="" for="'.$name.'">' : '<label class="col s4" for="'.$name.'">';
        $control .= $label.'</label>';
        $control .= $type == 1 ? '<input type="text" name="'.$name.'" id="'.$id.'" class="single-text" value="'.$value.'"> ': '<input type="text" class="col s8 single-text" name="'.$name.'" id="'.$id.'" value="'.$value.'">';
        $control .= '</div>';
        $control .= '</div>';
        return $control;
    }
    private function createEditor($name, $label, $id, $type, $value=""){
        $control = <<<EOT
        <div class="form-editor">
            <div class="row valign-wrapper">
                <div class="col s12">
                    <label>$label</label>
                </div>

            </div>
            <div class="row">
                <div class='col s12 pos-relative'>
                    <textarea id="$id" name="$name" class="editor hidden">$value</textarea>
                </div>
            </div>
        </div>
EOT;
        return $control;
    }
    private function createDate($name, $label, $id, $type, $value=""){

        $months = array("January",'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $monthPrint = '';
        $dayPrint = '';

        if($value != ""){
            $date = explode('-',$value);
        }else{
            $date = "";
        }
        for($i = 0; $i < count($months); $i++){
            $value = $i+1;
            if($date != "" && $date[1] == $value){
                $monthPrint .= "<option value='$value' selected>$months[$i]</option>";
            }else{
                $monthPrint .= "<option value='$value'>$months[$i]</option>";
            }
        }
        for($i = 1; $i < 31; $i++){
            $value = $i;
            if($date != "" && $date[2] == $value){
                $dayPrint .= "<option value='$value' selected>$i</option>";
            }else{
                $dayPrint .= "<option value='$value'>$i</option>";
            }
        }
        $year = "<option value='".date('Y')."'>".date('Y')."</option>";
        $control = <<<EOT
            <div class="row valign-wrapper">
                <div class="col s8 valign-wrapper">
                    <label>Date</label>
                </div>
                <div class="col s4 right-align">
                    <p class="right-align">
                     <input type="checkbox" class="filled-in" id="filled-in-box"/>
                     <label for="filled-in-box">Current Day</label>
                   </p>
                </div>
            </div>
            <div class="row date-container">
                <div class="input-field col s4">
                    <select name="$name-month" class="date-selector-month browser-default">
                        <option value="" disabled selected>Month</option>n
                        $monthPrint
                    </select>
                </div>
                <div class="input-field col s4">
                    <select name="$name-day" class="date-selector-day browser-default">
                        <option value="" disabled selected>Day</option>
                        $dayPrint
                    </select>
                </div>
                <div class="input-field col s4">
                    <select name="$name-year" class="date-selector-year browser-default">
                        <option value="" disabled selected>Year</option>
                        $year
                    </select>
                </div>
            </div>
EOT;
        return $control;
    }
    private function createUploader($name, $label, $id, $type, $value=""){
        $control = <<<EOT
        <div class="row uploader" id="$id">
            <div class="col s12"><label>$label</label></div>
            <input type="file" class="hidden upload-file" name="$name">
            <div class="col s8">
                <input type="text" class="upload-path" name="file-upload-label"  value="$value" readonly>
            </div>
            <div class="col s4">
                <button type="button" id="$name" class="browse-btn">BROWSE</button>
            </div>
        </div>
EOT;
        return $control;
    }
    private function createSlider($name, $label, $id, $type, $value){
        $control = <<<EOT
        <div class="row">
            <div class="col s1">
                <label>$label</label>
            </div>
            <div class="col s11">
                <p class="range-field">
                    <input type="range" id="$id" min="1" max="$value" class="range">
                </p>
            </div>
        </div>
EOT;
        return $control;
    }
    public function createButtons(){

    }
    public function createHidden($name, $id, $value){
        $control = '<input type="hidden" value="'.$value.'" name="'.$name.'" id="'.$id.'">';
        return $control;
    }
    public function createSolutionSelection($name, $id, $value,$label, $listValue = ""){
        $control = <<<EOT
        <div id="solution-main">
        <div class="row solution-container">
            <div class="col s12">
                <label>$label</label>
            </div>
            <div class="col s12">
                <div class="input-field">
                    <select class="content-type" name="$name">
                    <option value="1">IMAGE</option>
EOT;
                    $control .= $value == 2 ? '<option value="2" selected>GRAPHS</option>' : '<option value="2">GRAPHS</option>';
                    $control .= <<<EOT
                    </select>
                </div>
            </div>
        </div>
        <div clsas="row solution-container image-uploader">
EOT;
$control .= $this->createUploader('image-thumb', 'Background Image (300 DPI)', 'image-thumb', 1);
$control .= <<<EOT
        </div>
        <div class="row solution-container graph-container">
            <div class="col s12">
                <ul class="graph-selector">
                    <li><a href="">Pie</a></li>
                    <li><a href="">Bars</a></li>
                    <li><a href="">Lines</a></li>
                    <li class="add-btn"><a href="" class="add-graph-value">Add</a></li>
                </ul>
            </div>
        </div>
        <div class="row variable-list">
EOT;
$control .= $listValue;
$control .= <<<EOT
        </div>
        </div>
EOT;
        return $control;
    }
    public function createSolutionColor($name, $id, $value,$label, $listValue = ""){
        $control = <<<EOT
        <div id="solution-main">
            <div class="row solution-container">
                <div class="col s12">
                    <label>$label</label>
                </div>
                <div class="col s12">
                    <div class="input-field">
                        <select class="color-options" name="$name">
                            <option value="#25A9E0">Blue</option>
                            <option value="#161616">Black</option>
                            <option value="#929497">Gray</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
EOT;
        return $control;
    }
    public function renderForm($listValue = ""){
        $options = $this->options;
        $returnable = "";
        foreach($options as $option){
            if(!isset($option['value'])){
                $option['value']="";
            }
            switch($option['formType']){
                case 'text' :
                    $returnable .= $this->createSingleText($option['name'], $option['label'], $option['id'], $option['type'], $option['value'] ? $option['value'] : '');
                break;
                case 'textarea' :
                    $returnable .= $this->createEditor($option['name'], $option['label'], $option['id'], $option['type'], $option['value'] ? $option['value'] : '');
                break;
                case 'date' :
                    $returnable .= $this->createDate($option['name'], $option['label'], $option['id'], $option['type'], $option['value'] ? $option['value'] : '');
                break;
                case 'upload' :
                    $returnable .= $this->createUploader($option['name'], $option['label'], $option['id'], $option['type'], $option['value'] ? $option['value'] : '');
                break;
                case 'slider' :
                    $returnable .= $this->createSlider($option['name'], $option['label'], $option['id'], $option['type'], $option['value'] ? $option['value'] : '');
                break;
                case 'hidden' :
                    $returnable .= $this->createHidden($option['name'], $option['id'], $option['value']);
                break;
                case 'solutionSelect' :
                    $returnable .= $this->createSolutionSelection($option['name'], $option['id'], $option['value'],$option['label'], $listValue);
                break;
                case 'colorSelect' :
                    $returnable .= $this->createSolutionColor($option['name'], $option['id'], $option['value'],$option['label'], $listValue);
                break;
            }
        }
        return $returnable;
    }
    /*
    * Parameters
    * $data = array(
    *   id
    *   formType
    *   label
    *   id
    *   name
    *   value
    * )
    */
    public function createSegments($label){
        $returnable = "";
        $container = "";
        $i = 0;
        $options = $this->options;
        for($i = 0; $i < count($options); $i++){
            $returnable = "";
            $container .= '<div class="accordion" data-news-id="'.$options[$i]['news_id'].'">';
            $container .= '<div class="acc-label">'.date('F j, Y', strtotime($options[$i]['date_published'])).' : '.$options[$i]['news_content'][0]['value'].'<i class="fa fa-chevron-up" aria-hidden="true"></i></div>';
            $container .= '<div class="acc-content">';
            foreach( $this->options[$i]['news_content'] as $option){
                switch($option['formType']){
                    case 'text' :
                    $returnable .= $this->createSingleText($option['name'], $option['label'], $option['id'], $option['type'], $option['value']);
                    break;
                    case 'textarea' :
                    $returnable .= $this->createEditor($option['name'], $option['label'], $option['id'].$options[$i]['news_id'], $option['type'], $option['value']);
                    break;
                    case 'date' :
                    $returnable .= $this->createDate($option['name'], $option['label'], $option['id'], $option['type'], $option['value']);
                    break;
                    case 'upload' :
                    $returnable .= $this->createUploader($option['name'], $option['label'], $option['id'], $option['type'], $option['value']);
                    break;
                    case 'slider' :
                    $returnable .= $this->createSlider($option['name'], $option['label'], $option['id'], $option['type'], $option['value']);
                    break;
                }

            }
            $container .= $returnable;
            $container .= '</div></div>';
        }
        return $container;
    }

}
