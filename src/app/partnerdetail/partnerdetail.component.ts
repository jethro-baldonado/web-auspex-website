import { Component, trigger, state, style, transition, animate, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { PartnersService } from './../partners.service';
import { Partner } from './../partners/partners';
@Component({
  selector: 'app-partners',
  templateUrl: './partnerdetail.component.html',
  styleUrls: ['./partnerdetail.component.css'],
  providers : [PartnersService]
})
export class PartnerdetailComponent implements OnInit {
    partner : Partner;
    nextPartnerLink : string;
    selectedSlide : string;
    pageNumber : string;
  constructor(
      private router : Router,
      private activatedRoute : ActivatedRoute,
      private nav : NavbarService,
      private footer : FooterService,
      private partnersService : PartnersService
  ) { }

  ngOnInit() {
      this.nav.show();
      this.nav.pageTitle = "Contact Us";
      this.activatedRoute.params.subscribe((params: Params) => {
          let id = params['id'];
          this.partner = this.partnersService.getSinglePartner(id);
          this.nextPartnerLink = "/partners/" + this.partnersService.getNextPartner(id);
      });
  }
  scrollClick(){
      this.router.navigate(['/contact-us']);
  }
}
