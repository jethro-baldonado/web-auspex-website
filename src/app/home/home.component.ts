import { Component, OnInit } from '@angular/core';
import { FooterComponent } from './../footer/footer.component';
import { trigger, style, transition, animate, keyframes, query, stagger, state } from '@angular/animations';
import { NavigationService } from './../navigation.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations : [
      trigger('animateLogo', [
          state('in', style({
              transform : 'translateY(0)'
          })),
          transition('void => *', [
              style({ transform : 'translateY(-100%)' }),
              animate(500)
          ]),
          transition('* => void', [
              style({ transform : 'translateY(100%)' }),
              animate(500),
              query('h1', style({ opacity : 0,  transform : 'translateX(-75px)'})),

              query('h1', stagger('300ms', [
                  animate('800ms 1.2s ease-in', style({ opacity : 1, transform: 'translateX(0)' }))
              ]))
          ])
      ])
  ]
})
export class HomeComponent implements OnInit {

  constructor(private navigationService : NavigationService) { }

  ngOnInit() {
      this.navigationService.setNextPage('/ethos');
  }
}
