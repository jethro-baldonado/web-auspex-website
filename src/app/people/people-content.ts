import { People } from './../people/people';

export const PeopleContent : People[] = [
    {
        id : 1,
        name : 'Lauren Ipsum',
        position : 'Developer',
        description : `Developer at day, Tita's of alabang by night.
        Has an undying relationship with coffee but has Krispy Kreme as her side bae.`,
        background : '/assets/images/people1.png',
        content : [{
            page : 1,
            title : 'Bio',
            content : `Runner<br/>
            Foodie<br/>
            Sanrio Worshipper`,
            background: '/assets/images/people2.png'
        },
        {
            page : 2,
            title : 'Other Stuff',
            content : `Loves to watch home tv shopping<br/>
            #IsawIsLife #GuyandPipfoLyf<br/>
            #FiveRiceMinimumNineNapalaban`,
            background: '/assets/images/people1.png'
        }],
        nextMember : 1
    },
    {
        id : 2,
        name : 'Lauren asdfsadf',
        position : 'Developer',
        description : `Developer at day, Tita's of alabang by night.
        Has an undying relationship with coffee but has Krispy Kreme as her side bae.`,
        background : '/assets/images/people2.png',
        content : [{
            page : 1,
            title : 'Bio',
            content : `Runner<br/>
            Foodie<br/>
            Sanrio Worshipper`,
            background: '/assets/images/people2.png'
        },
        {
            page : 2,
            title : 'Other Stuff',
            content : `Loves to watch home tv shopping<br/>
            #IsawIsLife #GuyandPipfoLyf<br/>
            #FiveRiceMinimumNineNapalaban`,
            background: '/assets/images/people1.png'
        }],
        nextMember : 1
    },
    {
        id : 3,
        name : 'Lauren asdfdsafdsafsdaqwqwq',
        position : 'Developer',
        description : `Developer at day, Tita's of alabang by night.
        Has an undying relationship with coffee but has Krispy Kreme as her side bae.`,
        background : '/assets/images/people1.png',
        content : [{
            page : 1,
            title : 'Bio',
            content : `Runner<br/>
            Foodie<br/>
            Sanrio Worshipper`,
            background: '/assets/images/people2.png'
        },
        {
            page : 2,
            title : 'Other Stuff',
            content : `Loves to watch home tv shopping<br/>
            #IsawIsLife #GuyandPipfoLyf<br/>
            #FiveRiceMinimumNineNapalaban`,
            background: '/assets/images/people1.png'
        }],
        nextMember : 1
    }
];
