export class People {
    id : number;
    name : string;
    position : string;
    description : string;
    background : string;
    content : [{
        page : number,
        title : string,
        content : string,
        background : string
    }];
    nextMember : number;
}
