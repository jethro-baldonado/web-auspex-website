import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {trigger, state, animate, style, transition} from '@angular/animations';

import { routerTransition } from './../router.animations';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { PeopleService } from './../people.service';

import { People } from './../people/people';
import { PeopleContent } from './../people/people-content';
import { NavigationService } from './../navigation.service';

@Component({
    selector: 'app-people',
    templateUrl: './people.component.html',
    styleUrls: ['./people.component.css'],
    animations : [],
    host: {'[@routerTransition]': 'slideToTop'},
})
export class PeopleComponent implements OnInit {

    emps : People[];
    personId : number;
    totalPeople : number;

    leftColor : string;
    rightColor : string;
    prevPage : string;
    nextPage : string;
    state : string;
    index : number;
    translateValue : string;
    currentValue : number;
    transitionValue : string;
    fullPageWidth : string;
    slideWidth : string;
    constructor(private nav : NavbarService,
        private footer : FooterService,
        private peopleService : PeopleService,
        private navigationService : NavigationService
    ) { }

    ngOnInit() {
        this.nav.pageTitle = "Who We Are";
        this.nav.show();
        this.footer.setLink('/solutions/web');
        this.navigationService.setNextPage('/solutions/web');
        this.navigationService.setPrevPage('/global-presence');
        this.peopleService.getAllPeople().then(data => {
            this.peopleService.people = data;
            this.peopleService.peopleList = data;
            this.emps = data;
            this.totalPeople = this.emps.length + 1;
            this.personId = 1;
            this.index = 0;
            this.currentValue = 0;
            this.translateValue = '';
            this.transitionValue = "all 500ms ease";
            this.translateValue = "translateX(0%)";
            this.leftColor = "#929497";
            this.fullPageWidth = (this.totalPeople * 100) + "%";
            this.slideWidth = "calc(100% / " + this.totalPeople + ")";
        });

    }

    prev(){
        if(this.index > 0){
            this.rightColor = "#fff";
            this.index -=  1;
            this.transitionValue = "all 500ms ease";
            this.translateValue = "translateX(-" + ((100 / this.totalPeople) * this.index) + "%)";
            if(this.index - 1 <= 0){
                this.leftColor = "#929497";
            }
        }else{
            this.leftColor = "#929497";

        }
    }
    next(){
        if(this.index < this.totalPeople - 1){
            this.leftColor = "#fff";
            this.index += 1;
            this.transitionValue = "all 500ms ease";
            this.translateValue = "translateX(-" + ((100 / this.totalPeople) * this.index) + "%)";
            if(this.index == this.totalPeople - 1){
                this.rightColor = "#929497";
            }
        }
    }
    swipe(action) {
        if(action.direction == 2){
            this.next();
        }else if(action.direction == 4){
            this.prev();
        }
     }
}
