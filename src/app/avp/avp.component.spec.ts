import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvpComponent } from './avp.component';

describe('AvpComponent', () => {
  let component: AvpComponent;
  let fixture: ComponentFixture<AvpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
