import { Component, trigger, state, style, transition, animate, OnInit } from '@angular/core';
import { NavbarService } from './../navbar.service';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  animations: [
      trigger('slideInOut', [
          state('in', style({
              transform: 'translate3d(100%, 0, 0)'
          })),
          state('out', style({
              transform: 'translate3d(-315px, 0, 0)'
          })),
          transition('in => out', animate('400ms ease-in-out')),
          transition('out => in', animate('400ms ease-in-out'))
      ]),
  ]
})
export class NavbarComponent implements OnInit {

    constructor( public nav : NavbarService ) { }
    menuState:string = 'out';
    toggleMenu(){
        this.menuState = this.menuState === 'out' ? 'in' : 'out';
        console.log('test');
    }

  ngOnInit() {
  }

}
