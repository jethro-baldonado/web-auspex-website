export class Ethos {
    id : number;
    title : string;
    content : string;
    background : string;
    columns : string;
    permalink : string;
}
