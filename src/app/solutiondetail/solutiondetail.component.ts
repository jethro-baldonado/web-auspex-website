import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { routerTransition } from './../router.animations';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';

import { Solutions } from './../solutions/solutions';
import { SolutionsService } from './../solutions.service';
import { SolutionContentFrame } from './../solutions/solutions-frame';
import { NavigationService } from './../navigation.service';
@Component({
    selector: 'app-solutiondetail',
    templateUrl: './solutiondetail.component.html',
    styleUrls: ['./solutiondetail.component.css']
})
export class SolutiondetailComponent implements OnInit {

    solution;
    selectedSlide : number;
    pageNumber : number;
    fullPageWidth : string;
    slideWidth : string;
    translateValue : string;
    transitionValue : string;
    nextSolutionLink : string;
    nextSolutionId : number;
    currentMainPage : string;
    solutionType : string;
    solutionParent : string;

    nextSlide : number;
    prevSlide : number;
    leftColor : string;
    rightColor : string;

    constructor(public footer : FooterService,
        public nav : NavbarService,
        public solutionService : SolutionsService,
        public activatedRoute : ActivatedRoute,
        public navigationService : NavigationService
    ) { }

    ngOnInit() {
        this.navigationService.setScrollable(false);
        this.nav.pageTitle = "Solutions";
        this.nav.hide();
        this.selectedSlide = 1;
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['id'];
            let type = params['type'];
            this.currentMainPage = '/solutions/' + type;
            this.solutionService.getSingleSolution(id).then(data => {
                this.solution = data;
                for(var i = 0; i < this.solution.list.length; i++){
                    console.log(this.solution.list[i].chart_data)
                    if(this.solution.list[i].graph_type != 'image'){
                        this.solution.list[i].chart_data = JSON.parse(this.solution.list[i].chart_data);
                    }
                }

                this.pageNumber = this.solution.list.length;
                this.slideWidth = "calc(100% / " + this.pageNumber + ")";
                this.fullPageWidth = (this.pageNumber * 100) + '%';
                this.nextSolutionLink ='/solutions/'+ type + '/' + this.solutionService.getNextSolution(type, id);
                this.nextSolutionId = this.solutionService.getNextSolution(type, id);
                this.solutionParent = '/solutions/'+type;
                this.solutionType = type;
                this.nextSlide = 2;
                this.prevSlide = 0;
                if(this.pageNumber == this.selectedSlide){
                    this.selectedSlide = 1;
                }
            });
        });
    }
    slideNum(slide : number){
        if(slide != this.selectedSlide){
            if(slide > this.selectedSlide && slide <= this.pageNumber){
                this.transitionValue = "all 500ms ease";
                this.translateValue = "translateX(-" + ((100 / this.pageNumber) * (slide - 1)) + "%)";
                this.selectedSlide = slide;
                this.nextSlide++;
                this.prevSlide++;

            }
            else if(slide < this.selectedSlide && slide > 0 ){
                this.transitionValue = "all 500ms ease";
                this.translateValue = "translateX(-" + ((100 / this.pageNumber) * (slide - 1)) + "%)";
                this.selectedSlide = slide;
                this.nextSlide--;
                this.prevSlide--;
            }
        }
    }
    swipeSlide(action) {
        if(action.direction == 2){
            this.slideNum(this.selectedSlide + 1);
        }else if(action.direction == 4){
            this.slideNum(this.selectedSlide - 1);
        }
     }
    backToTop(){
        this.transitionValue = "all 500ms ease";
        this.translateValue = "translateX(0)";
        this.selectedSlide = 1;
    }
}
