import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolutiondetailComponent } from './solutiondetail.component';

describe('SolutiondetailComponent', () => {
  let component: SolutiondetailComponent;
  let fixture: ComponentFixture<SolutiondetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolutiondetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolutiondetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
