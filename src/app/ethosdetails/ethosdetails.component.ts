import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { routerTransition } from './../router.animations';
import { trigger, style, transition, animate, keyframes, query, stagger, state } from '@angular/animations';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';

import { Ethos } from './../ethos';
import { EthosService } from './../ethos.service';

import { NavigationService } from './../navigation.service';

@Component({
    selector: 'app-ethosdetails',
    templateUrl: './ethosdetails.component.html',
    styleUrls: ['./ethosdetails.component.css'],
    animations : [
        trigger('routerTransition', [
            state('toTop', style({position:'fixed', width:'100%', height:'100%', opacity : 1})),
            state('toBottom', style({position:'fixed', width:'100%', height:'100%', top: '0px', opacity : 1})),
            transition('toTop => void', [
                style({
                    opacity : 0,
                    transform : 'translateY(-100%)'
                })
            ]),
            transition('toBottom => void', [
                style({
                    opacity : 0,
                    transform : 'translateY(100%)'
                })
            ])
        ]),
        trigger('animateText', [
            transition('* => *', [
                query('h1', style({ opacity : 0,  transform : 'translateX(-75px)'})),
                query('p', style({ opacity : 0, transform: 'translateY(65px)' })),


                query('h1', stagger('300ms', [
                    animate('800ms 0.5s ease-in', style({ opacity : 1, transform: 'translateX(0)' }))
                ])),
                query('p', stagger('300ms', [
                    animate('800ms 0.5s ease-in', style({ opacity : 1, transform : 'translateY(0)' }))
                ]))
            ])
        ])
    ]
})
export class EthosdetailsComponent implements OnInit {

    title : string;
    bg : string;
    content : string;
    columns : string;
    ethosContent;
    selectedEthos : Ethos;
    ethosId : number;
    totalPages : number;

    leftColor : string;
    rightColor : string;
    nextSlide : number;
    prevSlide : number;
    nextAnimation : string;
    prevAnimation : string;

    selectedSlide : number;
    pageNumber : number;
    fullPageWidth : string;
    slideWidth : string;
    translateValue : string;
    transitionValue : string;

    animation : string;

    constructor(private nav : NavbarService,
        private footer : FooterService,
        private ethosService : EthosService,
        private activatedRoute : ActivatedRoute,
        private navigationService : NavigationService
    ) {  }

    /*getEthosContent(permalink) : void {
        this.ethosService.getAllEthos().then( value => {
            this.ethosContent = value;
            this.totalPages = this.ethosContent.length;
            this.selectedEthos = this.ethosContent.find( ethos => ethos.permalink === permalink );
            this.title = this.selectedEthos.title;
            this.bg = this.selectedEthos.background;
            this.content = this.selectedEthos.content;
            this.columns = this.selectedEthos.columns;
            this.ethosId = this.selectedEthos.id;
            if(this.ethosId > 1){
                this.prevPage = '/ethos/' + this.ethosContent[this.ethosId - 2].permalink;
                this.leftColor = "#fff";
            }else{
                this.prevPage = "null";
                this.leftColor = "#929497";
            }
            if(this.ethosId < 4){
                this.nextPage = '/ethos/' + this.ethosContent[this.ethosId].permalink;
                this.rightColor = "#fff";
            }else{
                this.nextPage = "null";
                this.rightColor = "#929497";
            }
        });
    }*/

    ngOnInit() {
        this.nav.pageTitle = "Who We Are";
        this.nav.show();
        this.footer.setLink('/global-presence');
        this.navigationService.setNextPage('/global-presence');
        this.navigationService.setPrevPage('/home');
        this.animation = this.navigationService.state;
        this.activatedRoute.params.subscribe((params: Params) => {
            let permalink = params['permalink'];
            console.log(this.ethosService.ethosContent);
            this.ethosContent = this.ethosService.ethosContent;
            this.pageNumber = this.ethosContent.length;
            this.slideWidth = "calc(100% / " + this.pageNumber + ")";
            this.fullPageWidth = (this.pageNumber * 100) + '%';
            this.selectedSlide = 1;
            this.nextSlide = 2;
            this.prevSlide = 0;
            if(this.prevSlide > 1){
                this.leftColor = "#fff";
            }else{
                this.leftColor = "#929497";
            }
            if(this.nextSlide < this.pageNumber){
                this.rightColor = "#fff";
            }else{
                this.rightColor = "#929497";
            }
        });
        
    }
    swipe(action) {

        if(action.direction == 2){
            this.slideNum(this.selectedSlide + 1);
        }else if(action.direction == 4){
            this.slideNum(this.selectedSlide - 1);
        }

     }
    slideNum(slide : number){
        if(slide < this.pageNumber + 1 && slide > 0){

            if(slide != this.selectedSlide){
                if(slide > this.selectedSlide){
                    this.transitionValue = "all 500ms ease";
                    this.translateValue = "translateX(-" + ((100 / this.pageNumber) * (slide - 1)) + "%)";
                    this.nextSlide++;
                    this.prevSlide++;
                }
                else if(slide < this.selectedSlide){
                    this.transitionValue = "all 500ms ease";
                    this.translateValue = "translateX(-" + ((100 / this.pageNumber) * (slide - 1)) + "%)";
                    this.nextSlide--;
                    this.prevSlide--;
                }
                this.selectedSlide = slide;

            }
        }
        if(this.selectedSlide > 1){
            this.leftColor = "#fff";
        }else{
            this.leftColor = "#929497";
        }
        if(this.selectedSlide < this.pageNumber){
            this.rightColor = "#fff";
        }else{
            this.rightColor = "#929497";
        }
    }
}
