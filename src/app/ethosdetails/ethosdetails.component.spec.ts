import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EthosdetailsComponent } from './ethosdetails.component';

describe('EthosdetailsComponent', () => {
  let component: EthosdetailsComponent;
  let fixture: ComponentFixture<EthosdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EthosdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EthosdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
