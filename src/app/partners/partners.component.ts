import { Component, trigger, state, style, transition, animate, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { PartnersService } from './../partners.service';
import { Partner } from './../partners/partners';
@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.css'],
  providers : [PartnersService]
})
export class PartnersComponent implements OnInit {
    partners : Partner[];
  constructor(
      private router : Router,
      private activatedRoute : ActivatedRoute,
      private nav : NavbarService,
      private footer : FooterService,
      private partnersService : PartnersService
  ) { }

  ngOnInit() {
      this.nav.show();
      this.nav.pageTitle = "Contact Us";
      this.partners = this.partnersService.getAllPartners();
      console.log(this.partners);
      this.footer.setLink('/contact-us');
  }
  partnerClick(id : number){
      this.router.navigate( ['/partners/'+ id] );
  }
}
