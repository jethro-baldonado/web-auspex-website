import { Partner } from './../partners/partners';

export const PartnersContent : Partner[] = [
    {
        id : 1,
        logo : "fa fa-slack",
        name : 'slack',
        description : `Whatever work means for you, Slack brings all the pieces and
people you need together so you can actually get things done.`
    },
    {
        id : 2,
        logo : "fa fa-amazon",
        name : 'amazon',
        description : `Cloud computing is the on-demand delivery of compute power, database storage, applications, and other IT resources through a cloud services platform via the internet with pay-as-you-go pricing.`
    },
    {
        id : 3,
        logo : "fa fa-pied-piper",
        name : 'pied piper',
        description : `Our revolutionary compression, mixed with industry standard encryption, makes Space Saver the perfect tool to lower server costs as well as keep client records safe. In addition to the limitless enterprise applications, Space Saver can be used on any device, so even your personal memory-clogged phones, laptops or tablets will be running like the day you got them. `
    },{
        id : 4,
        logo : "fa fa-stack-overflow",
        name : 'stack overflow',
        description : `Stack Overflow is the largest online community for programmers to learn, share their knowledge, and advance their careers.`
    }
];
