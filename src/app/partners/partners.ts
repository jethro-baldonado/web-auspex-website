export class Partner{
    id : number;
    logo : string;
    name : string;
    description : string;
}
