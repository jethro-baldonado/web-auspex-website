import { Injectable } from '@angular/core';
import { Career } from './careers/career';
import { CareerContent } from './careers/career-content';
@Injectable()
export class CareerService {
    getAllCareers() : Career[]{
        return CareerContent;
    }
    getSingleCareer(id : number) : Career{
        return CareerContent.find(x => x.id == id);
    }
    getNextCareer(id : number) : string{
        if(CareerContent.findIndex(x => x.id == id) < CareerContent.length - 1){
            return CareerContent[CareerContent.findIndex(x => x.id == id) + 1].permalink;
        }
        return '';
    }
    getPrevCareer(id : number) : string{
        console.log(CareerContent.findIndex(x => x.id == id));
        if(CareerContent.findIndex(x => x.id == id) > 0){
            return CareerContent[CareerContent.findIndex(x => x.id == id) - 1].permalink;
        }
        return '';
    }
    getCareerFromPermalink(perma : string) : Career{
        console.log(perma);
        return CareerContent.find(x => x.permalink == perma);
    }
}
