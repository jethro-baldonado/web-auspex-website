import { Component, OnInit } from '@angular/core';
import { FooterService } from './../footer.service';
import { NavbarService } from './../navbar.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  currentLink : string;
  constructor( public footerService : FooterService, public nav: NavbarService, public router : Router) { }

  ngOnInit() {
      this.currentLink = this.footerService.getLink();

  }
  scrollClick(){
      this.router.navigate([this.currentLink]);
  }
}
