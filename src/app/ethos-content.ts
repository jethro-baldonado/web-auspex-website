import { Ethos } from './ethos';

export const EthosContent: Ethos[] = [
    {
        id : 1,
        title : 'Mission',
        content : 'Our mission is to serve the people and businesses anywhere in the world by creating technological products that improve quality of life.',
        background : '/assets/images/mission.jpg',
        columns : 'col s12 m5',
        permalink : 'mission'
    },
    {
        id : 2,
        title : 'Vision',
        content : "Our aim is to pursue technological breakthroughs and innovations that gears towards improving humanity’s quality of life by delivering products and services that enhance business processes, overall lifestyle, game changing innovations and others. We aim to create positive contributions to society and the environment.",
        background : '/assets/images/landing.jpg',
        columns : 'col s12 m10 l6',
        permalink : 'vision'
    },
    {
        id : 3,
        title : 'Strength',
        content : "Our people is one of the most important assets of our company. Our leadership team comprises highly experienced individuals coming from various industries like banking and finance, engineering business process outsourcing, and information technology. We have a passionate and effective management team in synergy with highly motivated employees focused on innovation and driven by continuous process improvement.",
        background : '/assets/images/strength.jpg',
        columns : 'col s12 m8',
        permalink : 'strength'
    },
    {
        id : 4,
        title : 'Values',
        content : `1. Integrity above all else.<br/>
        2. Take care of our employees' welfare.<br/>
        3. Listen to and focus on our customers' user experience.<br/>
        4. Technology innovation is our life essence.<br/>
        5. Sustainable long term growth and profitability are key to our presence.<br/>
        6. Take every challenge as an opportunity to learn and succeed.<br/>
        7. Have fun while working.<br/>`,
        background : '/assets/images/values.jpg',
        columns : 'col s12 m9',
        permalink : 'values'
    }
];
