import { Injectable } from '@angular/core';

@Injectable()
export class FooterService {
    visible: boolean;
    _link : string;
    constructor() { this.visible = true; }
    hide() { this.visible = false; }
    show() { this.visible = true; }
    setLink(link : string){
        this._link = link;
    }
    getLink(){
        return this._link;
    }

}
