import { Injectable } from '@angular/core';

@Injectable()
export class NavigationService {

    isScrollable : boolean;
    nextPage : string;
    prevPage : string;
    state : string;
  constructor() {
      this.nextPage = '';
      this.prevPage = '';
      this.isScrollable = true;
      this.state = 'swipeDown';
  }

  setNextPage(link : string){
        this.nextPage = link;
  }
  setPrevPage(link : string){
      this.prevPage = link;
  }
  setScrollable(scroll : boolean){
      this.isScrollable = scroll;
  }

  getNextPage(){
      this.state = 'toTop';
      return this.nextPage;
  }
  getPrevPage(){
      this.state = 'toBottom';
      return this.prevPage;
  }
  getScrollable(){
      return this.isScrollable;
  }
}
