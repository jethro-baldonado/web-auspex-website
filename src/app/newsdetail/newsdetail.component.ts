import { Component, trigger, state, style, transition, animate, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { NewsService } from './../news.service';
import { News } from './../news/news';
import { NavigationService } from './../navigation.service';
import { MetaService } from '@ngx-meta/core';

@Component({
    selector: 'app-newsdetail',
    templateUrl: './newsdetail.component.html',
    styleUrls: ['./newsdetail.component.css'],
    animations : [
        trigger('slideInOut', [
            state('in', style({
                opacity: 0
            })),
            state('out', style({
                opacity:1
            })),
            transition('in => out', animate('200ms ease-in-out')),
            transition('out => in', animate('200ms ease-in-out'))
        ]),
    ],

})
export class NewsdetailComponent implements OnInit {
    year : string;
    month : string;
    day : string;
    news : News;
    actionShow : string;
    nextNews : string;
    prevNews : string;
    currentPage : string;
    constructor(private router : Router,
        private activatedRoute : ActivatedRoute,
        private nav : NavbarService,
        private footer : FooterService,
        private newsService : NewsService,
        private navigationService : NavigationService,
        private readonly meta : MetaService
    ) { }

    ngOnInit() {
        this.nav.hide();
        this.actionShow = "in";
        let date = new Date();
        let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November','December'];
        this.navigationService.setScrollable(false);
        this.activatedRoute.params.subscribe((params: Params) => {
            let permalink = params['permalink'];
            this.newsService.getSingleNews(permalink).subscribe(data => {
                this.news = data[0];
                this.nextNews = this.news['next'];
                this.prevNews = this.news['prev'];
                this.meta.setTitle(this.news.title);
                this.meta.setTag('og:image', this.news.thumb);
                this.meta.setTag('og:description', this.news.content.replace(/<\/?[^>]+(>|$)/g, "").substring(0,200));
                this.year = "2017"
                this.month = months[6];
                this.day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate() + '';

            });
            //let newsDate = this.news.datePublished.split("-");



        });
        this.currentPage = window.location.href;
    }
    showActionGroup(){
        this.actionShow = "out";
    }
    hideActionGroup(){
        this.actionShow = "in";
    }
}
