import { Injectable } from '@angular/core';
import { News } from './news/news';
import { NewsContent } from './news/news-content';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class NewsService {
    private newsUrl = "http://auspexstaging.auspex.com.ph/admin/api/get_current_news";
    private singleNews = "http://auspexstaging.auspex.com.ph/admin/api/get_single_news/";
    private archiveCount = "http://auspexstaging.auspex.com.ph/admin/api/get_archive_count";
    private archiveList = "http://auspexstaging.auspex.com.ph/admin/api/get_archive_list/"
    constructor(private http:Http){

    }
    getAllNews() : Promise<News[]>{
        return Promise.resolve(NewsContent);
    }
    getCurrentNews(){
        let date = new Date();
        let news = new Array();

        let yearMonth = date.getFullYear() + '-' + ("0" + (date.getMonth() + 1)).slice(-2);
        for(let i = 0; i < NewsContent.length; i++){
            if(NewsContent[i].yearMonth === yearMonth){
                news.push(NewsContent[i]);
            }
        }
        return this.http.get(this.newsUrl).map((res:Response) => res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

    }
    getNewsByArch(year : string){
        let news = new Array();
        let count = 0;
        let month = new Array();
        let yearMonth : string;
        for(let x = 1; x <= 12 ; x++){
            count = 0;
            let yearMonth = year + '-' + ("0" + (x)).slice(-2);
            for(let i = 0; i < NewsContent.length; i++){
                if(NewsContent[i].yearMonth === yearMonth){
                    count++;
                }
            }
            month.push(count);
        }
        return month;
    }
    getNewsArch(year : string, month : string){
        let news = new Array();

        let yearMonth = year + '-' + ("0" + (parseInt(month) + 1)).slice(-2);
        for(let i = 0; i < NewsContent.length; i++){
            if(NewsContent[i].yearMonth === yearMonth){
                news.push(NewsContent[i]);
            }
        }
        console.log(yearMonth);
        return news;
    }
    getSingleNews(news_id : number){
        return this.http.get(this.singleNews + news_id).map((res:Response) => res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    getArchiveCount(year : string){
        return this.http.get(this.archiveCount).map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    getArchiveList(year : string, month : string){
        return this.http.get(this.archiveList + year + '/' + month).map((res:Response) => res.json())
            .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    getNextNews(perma : string){

    }
    getPrevNews(perma : string){

    }
}
