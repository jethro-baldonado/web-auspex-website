import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

import { AvpComponent } from './avp/avp.component';
import { LandingComponent } from './landing/landing.component';
import { HomeComponent } from './home/home.component';
import { EthosComponent } from './ethos/ethos.component';
import { GlobalComponent } from './global/global.component';
import { EthosdetailsComponent } from './ethosdetails/ethosdetails.component';
import { PeopleComponent } from './people/people.component';
import { PeopledetailComponent } from './peopledetail/peopledetail.component';
import { SolutionsComponent } from './solutions/solutions.component';
import { SolutiondetailComponent } from './solutiondetail/solutiondetail.component';
import { NewsComponent } from './news/news.component';
import { NewsdetailComponent } from './newsdetail/newsdetail.component';
import { PartnersComponent } from './partners/partners.component';
import { ContactComponent } from './contact/contact.component';
import { CareersComponent } from './careers/careers.component';
import { CareerdetailComponent } from './careerdetail/careerdetail.component';
import { ArchiveComponent } from './archive/archive.component';
import { ArchivedetailComponent } from './archivedetail/archivedetail.component';
import { PartnerdetailComponent } from './partnerdetail/partnerdetail.component';

export const router: Routes = [
    {
        path : '',
        component : LandingComponent,
        data : {
            meta : {
                title : 'Auspex - See the future',
                description : 'A mobile app development company',
                'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
            }
        }
    },
    {
        path : 'avp',
        component : AvpComponent,
        data : {
            meta : {
                title : 'Auspex - See the future',
                description : 'A mobile app development company',
                'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
            }
        }
    },
    {
        path : 'home',
        component : HomeComponent,
        data : {
            meta : {
                title : 'Auspex - See the future',
                description : 'A mobile app development company',
                'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
            }
        }

    },
    { path : 'ethos', redirectTo: 'ethos/mission' },
    {
        path : 'ethos/:permalink',
         component : EthosdetailsComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'global-presence',
         component : GlobalComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'our-people',
         component : PeopleComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'our-people/:name',
         component : PeopledetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'solutions/:type',
         component : SolutionsComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'solutions/:type/:id',
         component : SolutiondetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'news' ,
         component : NewsComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'news/:permalink',
         component : NewsdetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'archive' ,
         component : ArchiveComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'archive/:year/:month',
         component : ArchivedetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    { path : 'partners', redirectTo : 'home' },
    { path : 'partners/:id', redirectTo : 'home' },
    {
        path : 'contact-us',
         component : ContactComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    { path : 'careers', redirectTo : 'home' },
    { path : 'careers/:title', redirectTo : 'home' }
];



export const routes: ModuleWithProviders = RouterModule.forRoot(router);
