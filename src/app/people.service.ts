import { Injectable } from '@angular/core';
import { People } from './people/people';
import { PeopleContent } from './people/people-content';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PeopleService {
    private allPeopleUrl = "http://www.auspex.com.ph/admin/api/get_employees";
    private singleEmpUrl = "http://www.auspex.com.ph/admin/api/get_single_emp/";
    public people;
    public peopleList : People[];
    public currentIdx : number = 0;
    constructor(private http:Http){
    }

    getAllPeople(): Promise<[People]>{
        return this.http.get(this.allPeopleUrl).toPromise().then(response => response.json());
    }
    getPeople() : People[]{
        return this.people;
    }
    getSinglePerson(id : number) : Promise<People>{
        return this.http.get(this.singleEmpUrl + id).toPromise().then(response => response.json());
    }
}
