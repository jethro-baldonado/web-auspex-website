import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { routerTransition } from './../router.animations';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { PeopleService } from './../people.service';

import { People } from './../people/people';
import { PeopleContent } from './../people/people-content';

@Component({
    selector: 'app-peopledetail',
    templateUrl: './peopledetail.component.html',
    styleUrls: ['./peopledetail.component.css'],
    animations : [],
    host: {'[@routerTransition]': 'slideToTop'},
})
export class PeopledetailComponent implements OnInit {

    person : People;
    selectedSlide : number;
    pageNumber : number;
    fullPageWidth : string;
    slideWidth : string;
    translateValue : string;
    transitionValue : string;
    nextMemberLink : string;
    nextMemberId : number;
    constructor(private nav : NavbarService, private footer : FooterService, private peopleService : PeopleService,private activatedRoute : ActivatedRoute) { }

    ngOnInit() {
        this.nav.pageTitle = "Our People";
        this.selectedSlide = 1;
        this.nav.hide();
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = params['name'];
            this.peopleService.getSinglePerson(id).then(data => {
                this.person = data;
                this.pageNumber = this.person.content.length;
                this.slideWidth = "calc(100% / " + this.pageNumber + ")";
                this.fullPageWidth = (this.pageNumber * 100) + '%';
                let idx = this.peopleService.peopleList.findIndex(data => data.id == id);
                this.peopleService.currentIdx = idx;
                if((idx + 1) < this.peopleService.peopleList.length){
                    let nextId = this.peopleService.peopleList[idx + 1].id;
                    this.nextMemberLink ='/our-people/' + nextId;
                    this.nextMemberId = nextId;
                    console.log(nextId);
                }else{
                    this.nextMemberId = 0;
                }
            });
        });
    }
    slideNum(slide : number){
        if(slide != this.selectedSlide){
            if(slide > this.selectedSlide && slide <= this.pageNumber){
                this.transitionValue = "all 500ms ease";
                this.translateValue = "translateX(-" + ((100 / this.pageNumber) * (slide - 1)) + "%)";
                this.selectedSlide = slide;
            }
            else if(slide < this.selectedSlide && slide > 0){
                this.transitionValue = "all 500ms ease";
                this.translateValue = "translateX(-" + ((100 / this.pageNumber) * (slide - 1)) + "%)";
                this.selectedSlide = slide;
            }

        }
    }
    backToTop(){
        this.transitionValue = "all 500ms ease";
        this.translateValue = "translateX(0)";
        this.selectedSlide = 1;
    }
    swipeSlide(action) {
        if(action.direction == 2){
            this.slideNum(this.selectedSlide + 1);
        }else if(action.direction == 4){
            this.slideNum(this.selectedSlide - 1);
        }
     }
}
