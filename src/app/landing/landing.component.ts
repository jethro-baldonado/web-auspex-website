import { Component, OnInit } from '@angular/core';
import { FooterComponent } from './../footer/footer.component';
import { FooterService } from './../footer.service';
import { NavigationService } from './../navigation.service';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
import { NavbarService } from './../navbar.service';
import { EthosService } from './../ethos.service';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
  animations : [
      trigger('animateLoad', [
        transition('* => *', [

            query('h1', style({opacity : 0, transform : 'translateX(40px)'})),
            query('.play-btn', style({ opacity : 0 , transform : 'translateY(-50px)'})),
            query('.bg',style({transform: 'scale(1)'})),

            query('.bg', stagger('300ms',[
                animate('800ms 1s ease-out', style({ transform: 'scale(1.1)'}))
            ])),
            query('h1', stagger('300ms', [
                animate('800ms 0.5s ease-out', style({opacity: 1, transform : 'translateX(0)'}))
            ])),
            query('.play-btn', stagger('300ms', [
                animate('800ms 0.5s ease-in', style({ opacity : 1, transform : 'translateY(0)' }))
            ]))
        ]),

      ])
  ]
})
export class LandingComponent implements OnInit {

  constructor(public footerService : FooterService, public naviService : NavigationService, public navService : NavbarService, public ethosService : EthosService) { }

  ngOnInit() {
      this.navService.hide();
      this.footerService.setLink('ethos');
      this.naviService.setNextPage('/ethos');
      this.naviService.setScrollable(true);
      this.ethosService.getAllEthos().subscribe(data => {
          this.ethosService.ethosContent = data;
      });
      this.ethosService.getGlobalPresence().subscribe(data => {
          this.ethosService.globalContent = data;
      });
  }

}
