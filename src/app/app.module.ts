import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import 'hammerjs';
import 'hammer-timejs';
import { routes } from './app.router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ShareButtonsModule } from 'ngx-sharebuttons';
import { HttpModule } from '@angular/http';
import { ChartModule } from 'angular2-chartjs';
import { ReactiveFormsModule } from '@angular/forms';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { MetaModule, MetaLoader, MetaStaticLoader, PageTitlePositioning } from '@ngx-meta/core';
//Components
import { AppComponent } from './app.component';
import { LandingComponent } from './landing/landing.component';
import { HomeComponent } from './home/home.component';
import { EthosComponent } from './ethos/ethos.component';
import { GlobalComponent } from './global/global.component';
import { EthosdetailsComponent } from './ethosdetails/ethosdetails.component';
import { PeopleComponent } from './people/people.component';
import { PeopledetailComponent } from './peopledetail/peopledetail.component';
import { SolutionsComponent } from './solutions/solutions.component';
import { SolutiondetailComponent } from './solutiondetail/solutiondetail.component';
import { NewsComponent } from './news/news.component';
import { NewsdetailComponent } from './newsdetail/newsdetail.component';
import { PartnersComponent } from './partners/partners.component';
import { ContactComponent } from './contact/contact.component';
import { CareersComponent } from './careers/careers.component';
import { CareerdetailComponent } from './careerdetail/careerdetail.component';
import { AvpComponent } from './avp/avp.component';
import { FooterComponent } from './footer/footer.component';
import { ArchiveComponent } from './archive/archive.component';
import { ArchivedetailComponent } from './archivedetail/archivedetail.component';
import { PartnerdetailComponent } from './partnerdetail/partnerdetail.component';

//Services

import { FooterService } from './footer.service';
import { NavbarComponent } from './navbar/navbar.component';
import { NavbarService } from './navbar.service';
import { EthosService } from './ethos.service';
import { PeopleService } from './people.service';
import { SolutionsService } from './solutions.service';
import { NewsService } from './news.service';
import { ClipboardModule } from 'ngx-clipboard';
import { ContactService } from './contact.service';
import { NavigationService } from './navigation.service';

export class HammerConfig extends HammerGestureConfig{
    overrides = <any>{
        'swipe' : { velocity : 0.5, threshold : 20, direction : 31 }
    }
}
export const router: Routes = [
    {
        path : '',
        component : LandingComponent,
        data : {
            meta : {
                title : 'Auspex - See the future',
                description : 'A mobile app development company',
                'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
            }
        }
    },
    {
        path : 'avp',
        component : AvpComponent,
        data : {
            meta : {
                title : 'Auspex - See the future',
                description : 'A mobile app development company',
                'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
            }
        }
    },
    {
        path : 'home',
        component : HomeComponent,
        data : {
            meta : {
                title : 'Auspex - See the future',
                description : 'A mobile app development company',
                'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
            }
        }

    },
    { path : 'ethos', redirectTo: 'ethos/mission' },
    {
        path : 'ethos/:permalink',
         component : EthosdetailsComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'global-presence',
         component : GlobalComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'our-people',
         component : PeopleComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'our-people/:name',
         component : PeopledetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'solutions/:type',
         component : SolutionsComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'solutions/:type/:id',
         component : SolutiondetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'news' ,
         component : NewsComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'news/:permalink',
         component : NewsdetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'archive' ,
         component : ArchiveComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    {
        path : 'archive/:year/:month',
         component : ArchivedetailComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    { path : 'partners', redirectTo : 'home' },
    { path : 'partners/:id', redirectTo : 'home' },
    {
        path : 'contact-us',
         component : ContactComponent,
         data : {
             meta : {
                 title : 'Auspex - See the future',
                 description : 'A mobile app development company',
                 'og:image' : 'http://auspex.com.ph/assets/images/home-page.png'
             }
         }
    },
    { path : 'careers', redirectTo : 'home' },
    { path : 'careers/:title', redirectTo : 'home' }
];
export function metaFactory(): MetaLoader {
  return new MetaStaticLoader({
    pageTitlePositioning: PageTitlePositioning.PrependPageTitle,
    pageTitleSeparator: ' - ',
    applicationName: 'Auspex Website',
    defaults: {
      title: 'See the future',
      description: 'A mobile app development company',
      'og:image' : 'http://auspex.com.ph/assets/images/home-page.png',
      'og:type': 'website',
      'og:locale': 'en_US',
      'og:locale:alternate': 'en_US,nl_NL,tr_TR'
    }
  });
}
@NgModule({
  declarations: [
    AppComponent,
    LandingComponent,
    HomeComponent,
    EthosComponent,
    GlobalComponent,
    EthosdetailsComponent,
    PeopleComponent,
    PeopledetailComponent,
    SolutionsComponent,
    SolutiondetailComponent,
    NewsComponent,
    NewsdetailComponent,
    PartnersComponent,
    ContactComponent,
    CareersComponent,
    CareerdetailComponent,
    AvpComponent,
    FooterComponent,
    NavbarComponent,
    ArchiveComponent,
    ArchivedetailComponent,
    PartnerdetailComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId : 'auspex-website'}),
    RouterModule.forRoot(router, { useHash : true }),
    BrowserAnimationsModule,
    ShareButtonsModule.forRoot(),
    HttpModule,
    ClipboardModule,
    ChartModule,
    ReactiveFormsModule,
    MetaModule.forRoot({
        provide : MetaLoader,
        useFactory : (metaFactory)
    })
  ],
  providers: [FooterService,
      NavbarService,
      PeopleService,
      SolutionsService,
      NewsService,
      ContactService,
      NavigationService,
      PeopleService,
      EthosService,
      { provide : HAMMER_GESTURE_CONFIG, useClass: HammerConfig }
   ],
  bootstrap: [AppComponent]
})
export class AppModule { }
