export class News{
    id : number;
    title : string;
    content : string;
    datePublished : string;
    thumb : string;
    isArchive : string;
    yearMonth : string;
    permalink : string;
    year : string;
    month : string;
    day : string;
}
