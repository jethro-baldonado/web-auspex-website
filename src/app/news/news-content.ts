import { News } from './../news/news';

export const NewsContent : News[] = [
    {
        id : 1,
        title : `Fire Response Integration`,
        content : `Put a bird on it listicle pork belly fap everyday carry letterpress.
        Tote bag biodiesel etsy, selfies kitsch blog shaman waistcoat. Cliche intelligentsia freegan,
        banh mi meditation paleo tumeric sartorial scenester.
        Put a bird on it listicle pork belly fap everyday carry letterpress.
        Tote bag biodiesel etsy, selfies kitsch blog shaman waistcoat. Cliche intelligentsia freegan,
        banh mi meditation paleo tumeric sartorial scenester.
        Put a bird on it listicle pork belly fap everyday carry letterpress.
        Tote bag biodiesel etsy, selfies kitsch blog shaman waistcoat. Cliche intelligentsia freegan,
        banh mi meditation paleo tumeric sartorial scenester.
        Put a bird on it listicle pork belly fap everyday carry letterpress.
        Tote bag biodiesel etsy, selfies kitsch blog shaman waistcoat. Cliche intelligentsia freegan,
        banh mi meditation paleo tumeric sartorial scenester.
        Put a bird on it listicle pork belly fap everyday carry letterpress.
        Tote bag biodiesel etsy, selfies kitsch blog shaman waistcoat. Cliche intelligentsia freegan,
        banh mi meditation paleo tumeric sartorial scenester.`,
        datePublished : '2017-06-27',
        thumb : '/assets/images/news_1.png',
        isArchive : 'false',
        yearMonth : '2017-06',
        permalink : '/news/fire-response-integration',
        year : '2017',
        month : 'Jan',
        day : '1'
    }
]
