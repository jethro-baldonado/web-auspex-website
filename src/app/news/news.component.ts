import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { NewsService } from './../news.service';
import { News } from './../news/news';
import { NavigationService } from './../navigation.service';


@Component({
    selector: 'app-news',
    templateUrl: './news.component.html',
    styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

    news : News[];
    visibility : boolean[];
    mobileVisibility : boolean[];
    year : string;
    month : string;
    day : string;
    pageNumber : number;
    selectedSlide : number;
    desktopPages : number;
    desktopSelected : number;

    leftColor : string;
    rightColor : string;
    prevPage : string;
    nextPage : string;
    index : number;
    translateValue : string;
    currentValue : number;
    transitionValue : string;
    fullPageWidth : string;
    slideWidth : string;

    constructor(private nav : NavbarService,
        private footer : FooterService,
        private newsService : NewsService,
        private router : Router,
        private navigationService : NavigationService
    ) { }

    ngOnInit() {
        let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November','December'];
        this.nav.show();
        this.nav.pageTitle = "What's New";
        this.footer.setLink('/partners');

        this.navigationService.setNextPage('/contact-us');
        this.navigationService.setPrevPage('/solutions/mobile');
        this.navigationService.setScrollable(false);
        let date = new Date();
        this.year = date.getFullYear() + "";
        this.month = months[date.getMonth()];
        this.day = date.getDate()+"";
        this.newsService.getCurrentNews().subscribe(data => {
            this.news = data;
            let visibleArr = new Array(this.news.length);
            this.mobileVisibility = new Array(this.news.length);
            for(let i = 0; i < this.news.length; i++){
                if(i == 0){
                    this.mobileVisibility[i] = true;
                }else{
                    this.mobileVisibility[i] = false;
                }
                if(i < 3){

                    visibleArr[i] = true;
                }else{

                    visibleArr[i] = false;
                }
            }
            this.visibility = visibleArr;
            this.pageNumber = this.news.length;
            if(this.pageNumber <= 3){
                this.desktopPages = 1;
            }else{
                this.desktopPages = Math.floor(this.pageNumber / 3);
                if((this.pageNumber % 3) > 0){
                    this.desktopPages++;
                }
            }
            this.selectedSlide = 1;
            this.desktopSelected = 1;
        });
        this.footer.setLink('/contact-us');
    }

    prev(){
        if(this.desktopSelected > 1){
            this.rightColor = "#fff";
            this.desktopSelected -=  1;
            let visiblePage = this.desktopSelected * 3;
            let articleAvailable = 3;
            if(this.pageNumber < 3){
                articleAvailable = this.pageNumber;
            }else{
                for(let i = 0; i < this.pageNumber; i++){
                    this.visibility[i] = false;
                    if(i >= (visiblePage - 3) && i < visiblePage){
                        this.visibility[i] = true;
                    }
                }
            }
            if(this.desktopSelected - 1 <= 0){
                this.leftColor = "#929497";
            }
        }else{
            this.leftColor = "#929497";

        }
    }
    next(){
        if(this.desktopSelected < this.desktopPages){
            this.leftColor = "#fff";
            this.desktopSelected += 1;
            let visiblePage = this.desktopSelected * 3;
            let articleAvailable = 3;
            if(this.pageNumber < 3){
                articleAvailable = this.pageNumber;
            }else{
                for(let i = 0; i < this.pageNumber; i++){
                    this.visibility[i] = false;
                    if(i >= (visiblePage - 3) && i < visiblePage){
                        this.visibility[i] = true;
                    }
                }
            }
            if(this.desktopSelected == this.desktopPages ){
                this.rightColor = "#929497";
            }
        }
    }
    prevArt(){
        if(this.selectedSlide > 1){
            this.rightColor = "#fff";
            this.selectedSlide -=  1;

            for(let i = 0; i < this.pageNumber; i++){
                this.mobileVisibility[i] = false;
                if(i == this.selectedSlide - 1){
                    this.mobileVisibility[i] = true;
                }
            }
            if(this.selectedSlide - 1 <= 0){
                this.leftColor = "#929497";
            }
        }else{
            this.leftColor = "#929497";

        }
    }
    nextArt(){
        if(this.selectedSlide < this.pageNumber){
            this.leftColor = "#fff";
            this.selectedSlide += 1;
            for(let i = 0; i < this.pageNumber; i++){
                this.mobileVisibility[i] = false;
                if(i == this.selectedSlide - 1){
                    this.mobileVisibility[i] = true;
                }
            }

            if(this.selectedSlide == this.pageNumber ){
                this.rightColor = "#929497";
            }
        }
    }
    swipe(action) {
        if(action.direction == 2){

            this.nextArt();
        }else if(action.direction == 4){

            this.prevArt();
        }
     }
}
