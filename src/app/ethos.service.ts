import { Injectable } from '@angular/core';
import { Ethos } from './ethos';
import { EthosContent } from './ethos-content';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class EthosService {
    private ethosUrl = 'http://auspexstaging.auspex.com.ph/admin/api/get_ethos/';
    private globalUrl = 'http://auspexstaging.auspex.com.ph/admin/api/get_global';
    public ethosContent : any[];
    public globalContent : any[];

    constructor(private http:Http){

    }
    getAllEthos(){
        return this.http.get(this.ethosUrl).map((res:Response) => res.json())
                         .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
    }
    getGlobalPresence(){
        return this.http.get(this.globalUrl).map((res:Response) => res.json())
                        .catch((error:any) => Observable.throw(error.json().error || 'Server Error'));
    }
}
