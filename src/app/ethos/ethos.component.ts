import { Component, OnInit } from '@angular/core';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';

import { Ethos } from './../ethos';
import { EthosService } from './../ethos.service';

@Component({
  selector: 'app-ethos',
  templateUrl: './ethos.component.html',
  styleUrls: ['./ethos.component.css']
})
export class EthosComponent implements OnInit {

  title : string;
  backgroundImage : string;
  content : string;
  columns : string;
  ethosContent : Ethos[];
  selectedEthos : Ethos;
  constructor(private nav : NavbarService, private footer : FooterService, private ethosService : EthosService) {  }

  getEthosContent() : void {
    
  }

  ngOnInit() {
      this.nav.show();
      this.footer.setLink('/global-presence');
      this.getEthosContent();
  }

}
