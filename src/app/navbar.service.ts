import { Injectable } from '@angular/core';

@Injectable()
export class NavbarService {
    visible: boolean;
    pageTitle : string;
    constructor() { this.visible = false; this.pageTitle = "";}
    hide() { this.visible = false; }
    show() { this.visible = true; }

}
