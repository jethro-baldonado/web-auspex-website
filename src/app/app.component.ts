import { Component} from '@angular/core';
import { NavbarService } from './navbar.service';
import { NavigationService } from './navigation.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  scrollable : boolean;
  constructor(public nav : NavbarService,
      private navigationService : NavigationService,
      private router : Router
  ){
      this.scrollable = this.navigationService.getScrollable();
  }
  swipe(action) {
      if(this.navigationService.getScrollable()){
          if(action.direction == 8){
              this.router.navigate([
                  this.navigationService.getNextPage()
              ]);
          }else if(action.direction == 16){
              this.router.navigate([
                  this.navigationService.getPrevPage()
              ]);
          }
      }
   }
 }
