import { Injectable } from '@angular/core';
import { Solutions } from './solutions/solutions';
import { SolutionContentFrame } from './solutions/solutions-frame';
import { SolutionsContent } from './solutions/solutions-content';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class SolutionsService {
    private solutionsUrl = "http://auspexstaging.auspex.com.ph/admin/api/get_solutions/";
    private singleSolutionUrl = "http://auspexstaging.auspex.com.ph/admin/api/get_single_solution/";
    constructor(private http:Http){}
    getSolutions(name : string){
        if(name == 'web'){
            return this.http.get(this.solutionsUrl + name).toPromise().then(response => response.json());
        }else if (name == 'mobile'){
            return this.http.get(this.solutionsUrl + name).toPromise().then(response => response.json());
        }else if (name == 'other'){
            return this.http.get(this.solutionsUrl + name).toPromise().then(response => response.json());
        }
    }
    getSingleSolution(id : number){
        return this.http.get(this.singleSolutionUrl + id).toPromise().then(response => response.json());
    }
    getNextSolution(type : string,id : number){
        /*if(this.getSolutions(type).findIndex( x => x.id == id) == this.getSolutions(type).length - 1){
            return 0;
        }else{
            return this.getSolutions(type)[this.getSolutions(type).findIndex( x => x.id == id) + 1].id;
        }*/
        return 1;
    }
}
