import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { NewsService } from './../news.service';
import { News } from './../news/news';

@Component({
  selector: 'app-archive',
  templateUrl: './archive.component.html',
  styleUrls: ['./archive.component.css']
})
export class ArchiveComponent implements OnInit {


  months : string[];
  newsPerMonth : {};

  constructor(private nav : NavbarService, private footer : FooterService, private newsService : NewsService, private router : Router) { }

  ngOnInit() {

      this.nav.show();
      this.nav.pageTitle = "What's New";
      this.months = ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November','December'];
      this.newsService.getArchiveCount('2017').subscribe(data => this.newsPerMonth = data);
      this.footer.setLink('/contact-us');
  }
  monthClick(month : number, news : number){
      let year : string = "2017";
      let link = '/archive/' + year + '/' + month;
      if(news > 0){
          this.router.navigate([link]);
      }
  }

}
