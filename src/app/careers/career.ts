export class Career{
    id : number;
    title : string;
    content : string;
    thumb : string;
    needed : number;
    permalink : string;
}
