import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { Career } from './../careers/career';
import { CareerService } from './../career.service';

@Component({
  selector: 'app-careers',
  templateUrl: './careers.component.html',
  styleUrls: ['./careers.component.css'],
  providers: [CareerService]
})
export class CareersComponent implements OnInit {

    careers : Career[];
  constructor(private nav : NavbarService,
      private footer : FooterService,
      private careerService : CareerService,
      private activatedRoute : ActivatedRoute) { }

  ngOnInit() {
      this.nav.show();
      this.nav.pageTitle = "Contact Us";
      this.careers = this.careerService.getAllCareers();
      console.log(this.careers);
  }
}
