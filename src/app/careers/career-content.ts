import { Career } from './../careers/career';

export const CareerContent : Career[] = [
    {
        id : 1,
        title : 'Jr. IOS Developer',
        content: `We are looking for an iOS developer responsible for the development and maintenance of applications aimed at a range of iOS devices including mobile phones and tablet computers. Your primary focus will be development of iOS applications and their integration with back-end services. You will be working alongside other engineers and developers working on different layers of the infrastructure. Therefore, a commitment to collaborative problem solving, sophisticated design, and the creation of quality products is essential.`,
        thumb : '/assets/images/job.png',
        needed : 3,
        permalink : 'jr-ios-developer'
    },
    {
        id : 2,
        title : 'Content Writer',
        content: `You can become a writer as a university graduate or a school leaver as there are no formal academic qualifications needed. Previous experience from published articles, freelance work or writing competitions can be useful, although not essential. A wide variety of institutions including universities, colleges and adult education centres offer short creative writing courses that can be helpful.`,
        thumb : '/assets/images/job.png',
        needed : 1,
        permalink : 'content-writer'
    }
];
