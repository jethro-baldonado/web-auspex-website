<div class="slide" *ngFor="let person of emps" [ngStyle]="{width : slideWidth}">
    <div class="bg" [ngStyle]="{'background-image': 'url('+ person.background +')'}"></div>
    <div class="content valign-wrapper">
        <div class="left-align color-white inner-content ethos">
            <div class="row">
                <div class="col s12 m12 l8">
                    <h1 class="left-align people-title">Our People</h1>
                    <h2 class="left-align person-name">{{ person.name }} / {{ person.position}}</h2>
                    <p innerHtml="{{ person.description }}" class="people-description" style="font-size: 1.5em;"></p>
                    <a [routerLink]="person.id" class="more-btn">More</a>
                </div>
            </div>
        </div>
    </div>
</div>
