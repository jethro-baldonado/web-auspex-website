import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class ContactService {
    constructor(private http : Http){}

    sendEmail(form : any){
        console.log(form);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post('/auspex-website/mailto.php', JSON.stringify(form), options).toPromise()
        .then(this.extractData)
        .catch(this.handleErrorPromise);
    }
    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }
    private handleErrorPromise (error: Response | any) {
        console.error(error.message || error);
        return Promise.reject(error.message || error);
    }
}
