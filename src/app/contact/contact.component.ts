import { Component, OnInit } from '@angular/core';
import { routerTransition } from './../router.animations';
import { FormBuilder, Validators } from '@angular/forms';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { ContactService } from './../contact.service';
import { NavigationService } from './../navigation.service';

@Component({
    selector: 'app-contact',
    templateUrl: './contact.component.html',
    styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
    submitted : boolean;
    constructor(private navbar : NavbarService,
        private footer : FooterService,
        private contactService : ContactService,
        private fb : FormBuilder,
        private navigationSerivce : NavigationService
    ) { }
    public contactForm = this.fb.group({
        email : [""],
        message : [""]
    })
    ngOnInit() {
        this.submitted = false;
        this.navbar.show();
        this.navbar.pageTitle = "Contact Us";
        this.navigationSerivce.setPrevPage('/news');
    }
    sendContact(event){
        this.contactService.sendEmail(this.contactForm.value);
        this.submitted = true;
    }
}
