import { Injectable } from '@angular/core';
import { Partner } from './partners/partners';
import { PartnersContent } from './partners/partner-contents';
@Injectable()
export class PartnersService {
    getAllPartners() : Partner[]{
        return PartnersContent;
    }
    getSinglePartner( id : number ) : Partner{
        return PartnersContent.find(x => x.id == id);
    }
    getNextPartner(id : number) : number{
        if(PartnersContent.findIndex( x => x.id == id) + 1 < PartnersContent.length){

            return PartnersContent[PartnersContent.findIndex( x => x.id == id) + 1].id;
        }
        return 0;
    }
}
