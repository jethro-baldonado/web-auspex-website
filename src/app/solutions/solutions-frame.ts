export class SolutionContentFrame {
    id : number;
    name : string;
    description : string;
    image : string[];
    behance : string;
    bgColor : string;
    pages : [{
        id : number,
        title : string,
        content : string,
        graphType : string,
        graphData : {
            labels : Array<string>,
            datasets: [{
                label : string,
                data : number[]
            }]
        }
    }];
};
