import { Solutions } from './../solutions/solutions';

export const SolutionsContent : Solutions = {
    'web' : [
        {
            id : 1,
            name : 'CMS',
            description : `This system is created for monitoring and making your business feasible. This can be personalized depending on your business nature.`,
            image : ['/assets/images/cms-login.png'],
            behance : 'https://www.behance.net/gallery/54502555/Blue-Med-Hospital-Management-System',
            bgColor : 'rgba(37,169,224,0.8)',
            pages : [{
                id : 1,
                title : 'Blue Med Center',
                content : `Neat and responsive, this system can monitor almost all that needs to be monitored in a certain business.`,
                graphType : 'image',
                graphData : {
                    labels : ['January', 'February', 'March', 'April', 'May', 'June', 'July','August', 'September', 'October', 'November','December'],
                    datasets : [{
                        label : '/assets/images/cms-dashboard.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1,5,1,7,3,4,6,1,2,6,1,5]
                    }]
                }
            },
            {
                id : 2,
                title : 'Blue Med Center',
                content : `Through this system, you can easily create reports and evaluate your business productivity.`,
                graphType : 'image',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : '/assets/images/cms-dashboard-2.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            }]
        },
        {
            id : 2,
            name : 'Fashion and E-commerce',
            description : `A must-have site built extensively for e-commerce business.`,
            image : ['/assets/images/hero.png'],
            behance : 'https://www.behance.net/gallery/54500913/Femme-Ecommerce-Concept-Design',
            bgColor : 'rgba(37,169,224,0.8)',
            pages : [{
                id : 1,
                title : 'Femme',
                content : `The design is simple and pleasing to the eye, making your products more glamorous and appealing.`,
                graphType : 'image',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : '/assets/images/fashion-page-1.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            },
            {
                id : 2,
                title : 'Femme',
                content : `Comprehensive, this can be tailored into various e-commerce trades—that is apt for your business.`,
                graphType : 'image',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : '/assets/images/fashion-page-2.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            }]
        }
    ],
    'mobile' : [
        {
            id : 1,
            name : 'Food Delivery App',
            description : `Food delivery is now made cool and easy through this classy and user-friendly app!`,
            image : ['/assets/images/food-hero.png', '/assets/images/food-page-2.png'],
            behance : 'https://www.behance.net/gallery/54647999/Silver-Bucket-Menu-Concept-Design',
            bgColor : '#929497',
            pages : [{
                id : 1,
                title : 'Silver Bucket',
                content : `Designed for food delivery, this app can help restaurants widen their reach in catering their delicacies to customers.`,
                graphType : 'image',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : '/assets/images/food-page-2.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    },{
                        label : '/assets/images/food-hero.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            },
            {
                id : 2,
                title : 'Silver Bucket',
                content : `Integrated with a management system, owners can monitor payments and see other details of the clients who use this app.`,
                graphType : 'image',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : '/assets/images/food-page-1.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    },
                    {
                        label : '/assets/images/food-page-2.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            }]
        },
        {
            id : 2,
            name : 'Reservation App',
            description : `This app is crafted elegantly for reservation purposes, such as restaurant reservation, hotel, and many more. `,
            image : ['/assets/images/reservation-login.png', '/assets/images/reservation-details.png'],
            behance : 'https://www.behance.net/gallery/54648345/Wild-Catch-Reservation-App-Concept-Design',
            bgColor : 'rgba(37,169,224,0.8)',
            pages : [{
                id : 1,
                title : 'Wild Catch',
                content : `Hassle-free, app users can make fast reservation anytime, anywhere! `,
                graphType : 'image',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : '/assets/images/reservation-login.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    },
                    {
                        label : '/assets/images/reservation-menu.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            },
            {
                id : 2,
                title : 'Wild Catch',
                content : `Sophisticated in style, this app gives a cozy feeling.`,
                graphType : 'image',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : '/assets/images/reservation-details.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    },
                    {
                        label : '/assets/images/reservation-menu.png',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            }]
        }
    ],
    'other' : [
        {
            id : 1,
            name : '911',
            description : `911, the universal number to respond in times of
            danger and is now here at our shores to do good`,
            image : ['/assets/images/hero.png'],
            behance : '',
            bgColor : '#020407',
            pages : [{
                id : 1,
                title : 'Stats and what nots',
                content : `In here we can put the
                description of app and what not but only a gist and is related to
                the image next to it`,
                graphType : 'doughnut',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label :'asdfsadf',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            },
            {
                id : 2,
                title : 'Stats and what nots Test 2',
                content : `In here we can put the
                description of app and what not but only a gist and is related to
                the image next to it`,
                graphType : 'doughnut',
                graphData : {
                    labels : ['Downloads'],
                    datasets : [{
                        label : 'asdfsadf',
                        backgroundColor : ['rgb(0,191,165)'],
                        borderColor : ['rgb(0,191,165)'],
                        data : [1]
                    }]
                }
            }]
        }
    ]
};
