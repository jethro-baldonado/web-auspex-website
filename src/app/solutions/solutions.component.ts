import { Component, OnInit } from '@angular/core';
import { SolutionsService } from './../solutions.service';
import { SolutionContentFrame } from './../solutions/solutions-frame';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { NavigationService } from './../navigation.service';

@Component({
  selector: 'app-solutions',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.css']
})
export class SolutionsComponent implements OnInit {

  solutionList;
  selectedSolution : number;
  totalSolution : number;
  solutionType : string;
  currentLink : string;
  leftColor : string;
  rightColor : string;
  prevPage : string;
  nextPage : string;
  state : string;
  index : number;
  translateValue : string;
  currentValue : number;
  transitionValue : string;
  fullPageWidth : string;
  slideWidth : string;
  constructor(public nav : NavbarService,
      public footer : FooterService,
      public solutionService : SolutionsService,
      public activatedRoute : ActivatedRoute,
      public router : Router,
      public navigationService : NavigationService
  ) { }

  ngOnInit() {
      this.selectedSolution = 1;
      this.nav.pageTitle = "Solutions";
      this.nav.show();

     this.activatedRoute.params.subscribe((params: Params) => {
         let type = params['type'];
         this.solutionService.getSolutions(type).then(data => {
             this.solutionList = data;
             console.log(data);
             this.totalSolution = this.solutionList.length;
             this.index = 0;
             this.currentValue = 0;
             this.translateValue = '';
             this.transitionValue = "all 500ms ease";
             this.translateValue = "translateX(0%)";
             this.leftColor = "#929497";
             this.fullPageWidth = (this.totalSolution * 100) + "%";
             this.slideWidth = "calc(100% / " + this.totalSolution + ")";
         });

         this.solutionType = type;
         if(this.solutionType == 'web'){
             this.currentLink = '/solutions/mobile';
             this.navigationService.setNextPage('/solutions/mobile');
             this.navigationService.setPrevPage('/our-people');

         }else if(this.solutionType == 'mobile'){
             this.currentLink = '/contact-us';
             this.navigationService.setNextPage('/news');
             this.navigationService.setPrevPage('/solutions/web');

         }else {
             this.currentLink = '/contact-us';
             this.navigationService.setNextPage('/news');
             this.navigationService.setPrevPage('/solutions/mobile');

         }
     });


  }
  prev(){
      if(this.index > 0){
          this.rightColor = "#fff";
          this.index -=  1;
          this.transitionValue = "all 500ms ease";
          this.translateValue = "translateX(-" + ((100 / this.totalSolution) * this.index) + "%)";
          if(this.index - 1 <= 0){
              this.leftColor = "#929497";
          }
      }else{
          this.leftColor = "#929497";

      }
  }
  next(){
      if(this.index < this.totalSolution - 1){
          this.leftColor = "#fff";
          this.index += 1;
          this.transitionValue = "all 500ms ease";
          this.translateValue = "translateX(-" + ((100 / this.totalSolution) * this.index) + "%)";
          if(this.index == this.totalSolution - 1){
              this.rightColor = "#929497";
          }
      }
  }
  swipeSlide(action) {
      if(action.direction == 2){
          this.next();
      }else if(action.direction == 4){
          this.prev();
      }else if(action.direction == 8){
          this.scrollClick();
      }else if(action.direction == 16){
          if(this.solutionType == 'web'){
              this.currentLink = '/our-people';
          }else if(this.solutionType == 'mobile'){
              this.currentLink = '/solution/web';
          }else {
              this.currentLink = '/solution/mobile';
          }
          this.router.navigate([this.currentLink]);
      }
   }
  scrollClick(){
      if(this.solutionType == 'web'){
          this.currentLink = '/solutions/mobile';


      }else if(this.solutionType == 'mobile'){
          this.currentLink = '/news';


      }else {
          this.currentLink = '/news';

      }
      this.router.navigate([this.currentLink]);
  }
}
