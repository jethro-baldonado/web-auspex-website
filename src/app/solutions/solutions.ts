export class Solutions {
    web : [{
        id : number,
        name : string,
        description : string,
        image : string[],
        behance : string,
        bgColor : string,
        pages : [{
            id : number,
            title : string,
            content : string,
            graphType : string,
            graphData : {
                labels : Array<string>,
                datasets: [{
                    label : string,
                    backgroundColor : string[],
                    borderColor: string[],
                    data : number[]
                }]
            }
        }]
    }];
    mobile : [{
        id : number,
        name : string,
        description : string,
        image : string[],
        behance : string,
        bgColor : string,
        pages : [{
            id : number,
            title : string,
            content : string,
            graphType : string,
            graphData : {
                labels : Array<string>,
                datasets: [{
                    label : string,
                    backgroundColor : string[],
                    borderColor: string[],
                    data : number[]
                }]
            }
        }]
    }];
    other : [{
        id : number,
        name : string,
        description : string,
        image : string[],
        behance : string,
        bgColor : string,
        pages : [{
            id : number,
            title : string,
            content : string,
            graphType : string,
            graphData : {
                labels : Array<string>,
                datasets: [{
                    label : string,
                    backgroundColor : string[],
                    borderColor: string[],
                    data : number[]
                }]
            }
        }]
    }];
};
