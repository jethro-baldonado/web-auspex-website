import { Component, trigger, state, style, transition, animate, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';

import { Career } from './../careers/career';
import { CareerService } from './../career.service';

@Component({
    selector: 'app-careerdetail',
    templateUrl: './careerdetail.component.html',
    styleUrls: ['./careerdetail.component.css'],
    providers : [CareerService],
    animations : [
        trigger('slideInOut', [
            state('in', style({
                opacity: 0
            })),
            state('out', style({
                opacity:1
            })),
            transition('in => out', animate('200ms ease-in-out')),
            transition('out => in', animate('200ms ease-in-out'))
        ]),
    ]
})
export class CareerdetailComponent implements OnInit {

    career : Career;
    nextCareer : string;
    prevCareer : string;
    actionShow : string;
    ones : string[];
    selectedCareerReqs : string;
    currentPage : string;
    constructor(private nav : NavbarService,
        private footer : FooterService,
        private careerService : CareerService,
        private activatedRoute : ActivatedRoute) { }

        ngOnInit() {
            this.ones = ['One','Two','Three','Four','Five','Six', 'Seven','Eight', 'Nine'];
            this.actionShow = "in";
            this.activatedRoute.params.subscribe((params: Params) => {
                let permalink = params['title'];
                this.career = (this.careerService.getCareerFromPermalink(permalink));
                console.log(this.career);
                this.selectedCareerReqs = this.ones[this.career.needed - 1];
                this.nextCareer = this.careerService.getNextCareer(this.career.id) == '' ? '/careers/' : '/careers/' + this.careerService.getNextCareer(this.career.id);
                this.prevCareer = this.careerService.getPrevCareer(this.career.id) == '' ? '/careers/' : '/careers/' + this.careerService.getPrevCareer(this.career.id);
            });
            this.currentPage = window.location.href;
        }
        showActionGroup(){
            this.actionShow = "out";
        }
        hideActionGroup(){
            this.actionShow = "in";
        }
    }
