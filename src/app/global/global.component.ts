import { Component, OnInit } from '@angular/core';
import { routerTransition } from './../router.animations';
import { NavbarService } from './../navbar.service';
import { FooterService } from './../footer.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { trigger, style, transition, animate, keyframes, query, stagger } from '@angular/animations';
import { NavigationService } from './../navigation.service';
import { EthosService } from './../ethos.service';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css'],
  animations : [
      trigger('animateText', [
          transition('* => *', [
              query('h1', style({ opacity : 0,  transform : 'translateX(-75px)'})),
              query('.bg', style({ transform : 'scale(1)' })),
              query('p', style({ opacity : 0, transform: 'translateY(65px)' })),

              query('.bg', stagger('300ms', [
                  animate('800ms 1s ease-in', style({ transform : 'scale(1.1)' }))
              ])),
              query('h1', stagger('300ms', [
                  animate('800ms 0.5s ease-in', style({ opacity : 1, transform: 'translateX(0)' }))
              ])),
              query('p', stagger('300ms', [
                  animate('800ms 0.5s ease-in', style({ opacity : 1, transform : 'translateY(0)' }))
              ]))
          ])
      ])
  ]
})
export class GlobalComponent implements OnInit {

  public globalContent : string;
  public background : string;
  constructor(private navbar : NavbarService,
      private footer : FooterService,
      private navigationService : NavigationService,
      private ethosService : EthosService
  ) { }

  ngOnInit() {
      this.navbar.show();
      this.navbar.pageTitle = "Who We Are";
      this.footer.setLink('/our-people');
      this.navigationService.setNextPage('/our-people');
      this.navigationService.setPrevPage('/ethos');
      this.globalContent = this.ethosService.globalContent['content'];
      this.background = this.ethosService.globalContent['background'];
  }

}
