<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Auspex</title>
    <base href="/">
    <meta property="og:image" content="<?php echo 'http://www.auspex.com.ph/admin'.$news->thumbnail ?>">
    <meta property="og:title" content="<?php echo $news->title; ?>">
    <meta property="og:description" content="<?php echo trim(preg_replace('/\s\s+/', ' ', substr(strip_tags($news->content),0,250))).'...'; ?>">
    <meta property="og:url" content="<?php echo 'http://www.auspex.com.ph/news/article/'.$news->news_id; ?>">
    <meta property="og:site_name" content="Auspex Corp">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="favicon.png">
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/js/fullpage/jquery.fullpage.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fonts/rotation/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/stylesheets/css/main.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="assets/js/fullpage/jquery.fullpage.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
    <style>
        .prev-article, .next-article{
            position: absolute;
            top: 10%;
            padding: 0px 5px;
            width: 130px;
            height: 30px;
            line-height: 30px;
            background:#1B75bb;
            z-index: 999999;
        }
        .prev-article a, .next-article a{
            color: #fff !important;
            display: inline-block;
            width: 100%;
            height: 100%;
            font-size: 0.875em;
        }
        .prev-article a i, .next-article a i{
            font-size: 1.25em;
            line-height: 30px;
        }
        .prev-article.disabled, .next-article.disabled{
            background: #929497;
        }
        .prev-article a i{
            margin-right: 5px;
        }
        .next-article a i{
            margin-left: 5px;
        }
        .prev-article{
            left: 0px;
            padding-left: 10px;
        }
        .next-article{
            right: 0px;
            padding-right: 10px;
        }
        .float-button{
            width: 100px;
            height: 100px;
            background: #24b0d1;
            text-align: center;
            border-radius: 50%;
            position: absolute;
            bottom: 5%;
            right: 2%;
            z-index: 999999;
            font-size: 3em;
            padding: 20px;
        }
        .action-group a{
            font-size: 2em;
        }
        #toast-container {
          top: auto !important;
          right: auto !important;
          bottom: 10%;
          left:45%;
        }
        .back-to-top-news-l{
            margin-top: 30px;
            height: 30px;
            width: 100%;
            text-align:center;
            background: #1B75BB;
            color: #fff;
            font-size: 1.25em;
        }
        .back-to-top-news-l a{
            display: inline-block;
            height: 30px;
            line-height: 30px;
            width: 100%;
            color: #fff;
            font-size: 0.875em;
        }
        @media only screen and (max-width : 480px) and (orientation : portrait){
            .float-button{
                width: 50px;
                height: 50px;
                opacity: 0.8;
                padding:10px;
            }
            .content{
                height: 90% !important;
                top: 0 !important;
            }
            .back-to-top-news{
                height: 10%;
                width: 100%;
                text-align:center;
                background: #1B75BB;
                color: #fff;
                font-size: 1.25em;
                position: fixed;
                bottom: 0%;
            }
            .back-to-top-news a{
                color: #fff;
                line-height: 85px;
                display: inline-block;
                width: 100%;
                text-align: center;
            }
            .news-detail-container{
                height: 100%;
            }
            .prev-article, .next-article{
                top: 0px;
                width: 90px;
            }
            #toast-container {
              top: auto !important;
              right: auto !important;
              bottom: 10%;
              left: 33%;
              min-width: 30%;
            }
            .action-group a{
                font-size: 2.5em;
            }
        }
        @media only screen and (min-width : 481px) and (max-width : 520px){
            .float-button{
                width: 50px;
                height: 50px;
                opacity: 0.8;
                padding:10px;
            }
            .content{
                height: 90% !important;
                top: 0 !important;
            }
            .back-to-top-news{
                height: 10%;
                width: 100%;
                text-align:center;
                background: #1B75BB;
                color: #fff;
                font-size: 1.25em;
                position: fixed;
                bottom: 0px;
            }
            .back-to-top-news a{
                color: #fff;
                line-height: 85px;
                display: inline-block;
                width: 100%;
                text-align: center;
            }
            .news-detail-container{
                height: 100%;
            }
            .prev-article, .next-article{
                top: 0px;
                width: 90px;
            }
            #toast-container {
              top: auto !important;
              right: auto !important;
              bottom: 10%;
              left: 33%;
              min-width: 30%;
            }
            .action-group a{
                font-size: 2.5em;
            }
        }
        @media only screen and (min-width : 521px) and (max-width : 1024px){
            .float-button{
                width: 50px;
                height: 50px;
                opacity: 0.8;
                padding:10px;
            }
            .content{
                height: 90% !important;
                top: 0 !important;
            }
            .back-to-top-news{
                height: 10%;
                width: 100%;
                text-align:center;
                background: #1B75BB;
                color: #fff;
                font-size: 1.25em;
                position: fixed;
                bottom: 0px;
            }
            .back-to-top-news a{
                color: #fff;
                line-height: 85px;
                display: inline-block;
                width: 100%;
                text-align: center;
            }
            .news-detail-container{
                height: 100%;
            }
            .prev-article, .next-article{
                top: 0px;
                width: 90px;
            }
            #toast-container {
              top: auto !important;
              right: auto !important;
              bottom: 10%;
              left: 33%;
              min-width: 30%;
            }
            .action-group a{
                font-size: 2.5em;
            }
        }
    </style>
</head>
<body>

    <div class="full center-align" style="position: fixed; z-index:999999; background: #fff;width: 100%;height:100%;padding-top:20%;">
        <span class="icon-landscape" style="font-size: 10em;margin-top:auto;margin-bottom:auto;"></span>
        <h1 style="font-size: 2em;">Rotate your device</h1>
    </div>
    <div class="wrapper news-detail-component">
        <div class="bg" style="background-image:url('http://www.auspex.com.ph/assets/images/news.png')"></div>
        <div class="col s12 m2 top-controls hide-on-large-only">
        </div>

        <div class="prev-article left-align <?php echo count($prev) ? '' : 'disabled' ?>"><a href="<?php echo count($prev) ? base_url('article').'/'.$prev->news_id : ''; ?>" <?php echo !count($prev) ? 'onclick="return false;"' : ''; ?>><i class="fa fa-angle-left" aria-hidden="true"></i> Previous News</a></div>
        <div class="next-article right-align <?php echo count($next) ? '' : 'disabled' ?>"><a href="<?php echo count($next) ? base_url('article').'/'.$next->news_id : '#'; ?>" <?php echo !count($next) ? 'onclick="return false;"' : ''; ?>>Next News <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
        <div class="content">
            <div class="row" style="width: 100%;">
                <div class="col s12 m12 l2 hide-on-med-and-down">

                </div>
                <div class="col s12 m12 l8">
                    <div class="news-detail-container">
                        <div class="thumb">
                            <img src="<?php echo 'http://www.auspex.com.ph/admin'.$news->thumbnail ?>"/>
                        </div>
                        <div class="title">
                            <h2 style="margin-bottom: 0px;"><?php echo $news->title; ?></h2>
                            <div class="col s12" style="padding-left:  0px;margin-bottom: 30px;">
                                <?php echo date('F j, Y', strtotime($news->date_published)); ?>
                            </div>
                            <div class="col s12" style="padding-left: 0px;">
                                <div class="action-group" [@slideInOut]="actionShow">
                                    <a href="#" class="copyClipboard" data-clipboard-text="<?php echo htmlentities('http://www.auspex.com.ph/news/article/'.$news->news_id); ?>" data-tooltip="Copied to clipboard!" data-position="bottom">
                                        <span class="icon-copy"></span>
                                    </a>
                                    <a id="facebook-share" href="<?php
                                    echo "https://www.facebook.com/dialog/share?app_id=1703417229953120&display=popup&href=".htmlentities('http://www.auspex.com.ph/news/article/'.$news->news_id)."&redirect_uri=".htmlentities('http://www.auspex.com.ph/news/article/'.$news->news_id);
                                    ?>">
                                        <span class="icon-facebook"></span>
                                    </a>
                                    <a href="http://twitter.com/intent/tweet?text=<?php echo $news->title; ?>&url=<?php echo htmlentities('http://www.auspex.com.ph/news/article/'.$news->news_id); ?>">
                                        <span class="icon-twitter"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div style="clear:both;"></div>
                        <div class="inner-content">
                            <p>
                                <?php echo $news->content; ?>
                            </p>

                        </div>
                        <div class="row" style="margin-bottom: 0px;">
                            <div class="back-to-top-news-l col s12 hide-on-small-only valign-wrapper align-center">
                                <a href="">Back to top</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="back-to-top-news col s12 hide-on-med-and-up valign-wrapper align-center">
            <a href="">Back to top</a>
        </div>
        <div class="float-button">
            <a href="http://www.auspex.com.ph/#/news"><span class="icon-auspex-home"></span></a>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/web-animations/2.2.5/web-animations.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.7.1/clipboard.min.js"></script>

    <script>
        var clipboard = new Clipboard('.copyClipboard');
        $(document).ready(function(){
            $('.copyClipboard').click(function(){
                event.preventDefault();
                Materialize.toast('Copied to clipboard!', 1000);
            });
            $('.back-to-top-news-l a, .back-to-top-news a').click(function(){
                event.preventDefault();
                $(".news-detail-container").animate({ scrollTop: (0) }, 'slow');
            })
        })
    </script>
</body>
</html>
