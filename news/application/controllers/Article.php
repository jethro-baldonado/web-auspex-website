<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
     public function _remap($param){
         $this->index($param);
     }
    public function index($id=0){

        $this->load->model('news_model');
        $news = $this->news_model->get_single($id);
        if($news){
            $next = count($this->news_model->get_next($id)) > 0 ? $this->news_model->get_next($id) : 0;
            $prev = count($this->news_model->get_prev($id)) > 0 ? $this->news_model->get_prev($id) : 0;
            $data = array('news' => $news[0], 'next' => $next[0], 'prev' => $prev[0]);
            $this->load->view('news',$data);
        }else{

        }
    }
}
