import { AuspexPage } from './app.po';

describe('auspex App', () => {
  let page: AuspexPage;

  beforeEach(() => {
    page = new AuspexPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
